unit UCommons;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, IniFiles,
  {$IFDEF WINDOWS} registry,{$ENDIF}
  ZConnection, db, ZDataset, ZCompatibility, Controls, StdCtrls, Printers, Dialogs, uRegExpr;

type
  TVTEnum = (vtInvalid=-1,vtInt=0,vtFloat,vtString,vtArray);
  TStrRowObj = class
    id:integer;
    vt:TVTEnum;
  end;

  TVTRetType = record
    Value: UTF8String;
    robj: TStrRowObj;
  end;

  {* Represent a db Primary key, index unique for stringrids *}
  TIntObj = class
    private
      FValue: int64;
    public
      property Value: int64 read FValue write FValue;
  end;

  TTotJournal = class
    public
      tpLn: string;
      TotHT,TotTTC,TotTVA,TotRemise,TotAcompte: extended;
      constructor Create(typeLine: string);
      function toString():string;
  end;

  TFactureEtat = (feImpayee=0,fePayee,feEncours,feDevis);
  TBaseFacturations = class
    private
      FIni: TIniFile;
      FCfg: TStringList;
      FZConn: TZConnection;
      FRe: TRegExpr;
      FAppFolder,FBinPath,FUserHomePath,FDocPath: string;
      zqlid: TZReadOnlyQuery;
      FIsCustom : boolean;
    protected
      function LoadFileConfig (): boolean;
      function LoadDBConfig (): boolean;
      function SaveFileConfig(): boolean;
      function SaveDBConfig(): boolean;
      function EscapeStr(value:string):string;
      function UnescapeStr(value:string):string;
    public
      varTypes: array[0..3] of string[16];
      formatters: TFormatSettings;
      constructor Create();
      destructor Destroy(); override;
      function ConvToLocalDecSep(const inputStr: string):string;
      procedure SetCfgVal(key, value: String; ro: TStrRowObj);
      procedure SetCfgVal(key, value: String);
      function GetCfgVal(key: string): string;
      function GetDBCfgVal(key: string): TVTRetType;
      function RemCfgVal(key: string; ro: TStrRowObj): boolean;
      function GetCfgValues(): TStringList;
      function MeasureText(Text: string; Width: LongInt): integer;
      function LastInsertID(const TableName: String): Integer;
      property ZConn: TZConnection read FZConn;
      property AppPath:string read FAppFolder;
      property BinPath:string read FBinPath;
      property UserHomePath:string read FUserHomePath;
  end;

function FillListComponentFormat(var zq: TZReadOnlyQuery;var  wc:TWinControl; formatter: string): boolean;

var BaseFact: TBaseFacturations;

implementation

constructor TTotJournal.Create(typeLine:string);
begin
  inherited Create;
  tpLn:=typeLine;
  TotHT:=0;
  TotTTC:=0;
  TotRemise:=0;
  TotTVA:=0;
  TotAcompte:=0;
end;

function TTotJournal.toString():string;
begin
  result := 'Type='+self.tpLn+', TTC='+FloatToStr(self.TotTTC)+', Acompte='+FloatToStr(self.TotAcompte)+', Remise='+FloatToStr(self.TotRemise);
end;

constructor TBaseFacturations.Create();
var dbpath: string;
  zq: TZQuery;
begin
  try
    varTypes[0] := 'Entier';
    varTypes[1] := 'Flottant';
    varTypes[2] := 'Chaîne';
    varTypes[3] := 'Sérialisation';
    FIsCustom:=false;
    LoadFileConfig();
    FCfg.OwnsObjects:=true;
    FZConn := TZConnection.Create(nil);
{$IFDEF MSWINDOWS}
   {$IFDEF WIN32}
    FZConn.LibraryLocation := FBinPath+DirectorySeparator+'sqlite3_x32.dll';
   {$ELSE}
   FZConn.LibraryLocation := FBinPath+DirectorySeparator+'sqlite3_x64.dll';
   {$ENDIF}
   FZConn.UTF8StringsAsWideField:=true;  //Compat linux???
{$ENDIF}
    FZConn.Protocol := 'sqlite-3';
    FZConn.ClientCodepage:='UTF-8';
    FZConn.ControlsCodePage:=cCP_UTF8;
    FZConn.AutoCommit := false;

    dbpath := GetCfgVal('dbpath');
    if (not FIsCustom and (dbpath = '')) then
    begin
      dbpath := FUserHomePath + DirectorySeparator +
      {$IFDEF WINDOWS}'AppData\Roaming\MiniFact'{$ELSE}'.minifact'{$ENDIF}
      + DirectorySeparator;
    end else if dbpath = '' then
      dbpath := FUserHomePath + DirectorySeparator;

    FZConn.Database := dbpath  + 'facts.db';
    if not FileExists(dbpath + 'facts.db') then
    begin
      FZConn.Connect();

      zq := TZQuery.Create(nil);
      zq.Connection := FZConn;
      zq.SQL.LoadFromFile(FAppFolder + DirectorySeparator + 'schema.sql');
      zq.ExecSQL;
      zq.Close;
      zq.Free;
      ShowMessage('La base de donnée n''a pu être chargé!'+#13#10
        +'Le fichier DB a été créé dans le répertoire de travail.');
    end else FZConn.Connect();

    LoadDBConfig();

    zqlid := TZReadOnlyQuery.Create(nil);
    zqlid.Connection := self.ZConn;
    except on e:Exception do
    begin
      ShowMessage(e.Message);
      Application.Terminate;
    end;
  end;
end;

destructor TBaseFacturations.Destroy();
begin
  SaveDBConfig();
  SaveFileConfig();
  if Assigned(FIni) then
    FIni.Free;
  if Assigned(FCfg) then
    FCfg.Free;
  if Assigned(FZConn) then
    FZConn.Free;
end;

function TBaseFacturations.EscapeStr(value:string):string;
begin
  result := StringReplace(value,#13,'#13',[rfReplaceAll]);
  result := StringReplace(result,#10,'#10',[rfReplaceAll]);
end;

function TBaseFacturations.UnescapeStr(value:string):string;
begin
  result := StringReplace(value,'#13',#13,[rfReplaceAll]);
  result := StringReplace(result,'#10',#10,[rfReplaceAll]);
end;

function TBaseFacturations.LoadDBConfig (): boolean;
var
  sect, sectkvz: TStringList;
  fn,fpath: string;
  i: integer;
  zqrConf: TZReadOnlyQuery;
  ro: TStrRowObj;
begin
  try
    zqrConf := TZReadOnlyQuery.Create(nil);
    zqrConf.Connection := ZConn;
    zqrConf.SQL.Text:= 'SELECT id,vt,nom,valeur FROM config;';
    zqrConf.ExecSQL;
    zqrConf.Open;
    zqrConf.First;
    while not zqrConf.EOF do
    begin
      ro := TStrRowObj.Create;
      ro.vt := TVTEnum(zqrConf.Fields[1].AsInteger);
      ro.id := zqrConf.Fields[0].AsInteger;
      i := FCfg.AddObject(zqrConf.Fields[2].AsString+'='+zqrConf.Fields[3].AsString,ro);
      zqrConf.Next;
    end;
    zqrConf.Close;
    zqrConf.Free;
    except on e:Exception do
    begin
      ShowMessage(e.Message);
//      Application.Terminated := true;
    end;
  end;
end;

function TBaseFacturations.LoadFileConfig (): boolean;
var
  sect, sectkvz: TStringList;
  fn,fpath,pdfpath: string;
  i,i2: integer;
  {$IFDEF WINDOWS} reg: TRegistry;{$ENDIF}
begin
  try
    fn := ExtractFileName(Application.ExeName);
{$IFDEF WINDOWS}
    fn := Copy(fn,1,Pos('.',fn)-1);
    FUserHomePath := GetEnvironmentVariable('USERPROFILE');
    reg := TRegistry.Create(HKEY_CURRENT_USER);
    FAppFolder := GetEnvironmentVariable('ProgramFiles')+ '\' + fn;
    FBinPath := GetEnvironmentVariable('ProgramFiles')+ '\' + fn;
    if reg.OpenKeyReadOnly('\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders') then
      FDocPath := reg.ReadString('Personal');
    reg.CloseKey;
    reg.Free;
{$ELSE IFDEF LINUX}
    FUserHomePath := GetEnvironmentVariable('HOME')+DirectorySeparator+'.minifact'+ DirectorySeparator;
    FAppFolder := '/usr/share/minifact';
    FBinPath := '/usr/bin';
    FDocPath := GetEnvironmentVariable('HOME')+DirectorySeparator+'Documents';
{$ENDIF}
//En mode USB tout est au meme niveau
    if strpos(PChar(Application.ExeName),PChar(FBinPath))=nil then
    begin
      FUserHomePath := '.';
      FBinPath:='.';
      FAppFolder:='.';
      FDocPath := '.';
      FIsCustom:=true;
    end;

    if not FIsCustom then
    begin
    {$IFDEF WINDOWS}
      fpath:=FUserHomePath+'\AppData\Roaming\'+fn+DirectorySeparator;
      pdfpath := FDocPath+DirectorySeparator+'MiniFactPDF';
    {$ELSE IFDEF LINUX}
      fpath:=FUserHomePath+DirectorySeparator;
      pdfpath := FDocPath+DirectorySeparator+'MiniFactPDF';
    {$ENDIF}
      if not DirectoryExists(fpath) then CreateDir(fpath);
      if not DirectoryExists(pdfpath) then CreateDir(pdfpath);
    end else
    begin
      fpath:=FUserHomePath+DirectorySeparator;
      pdfpath := FUserHomePath+DirectorySeparator+'MiniFactPDF';
      if not DirectoryExists(pdfpath) then
        if not CreateDir(pdfpath) then
          ShowMessage('Ne peut pas créer le dossier de sortie des PDF dans "' + GetCfgVal('pdfrep')+'".');
    end;
    //    ShowMessage(FAppFolder+DirectorySeparator+fn+#13#10+FAppFolder + DirectorySeparator + 'facts.db');

    if not FileExists(fpath+fn+'.ini') then
    begin
      FIni := TIniFile.Create(fpath+fn+'.ini' );
      FIni.WriteString('main','pdfrep',pdfpath);
      FIni.WriteString('main','dbpath', fpath);
      FIni.UpdateFile;
      ShowMessage('Le fichier de configuration des informations de la société n''a pu être chargé!'+#13#10
                            +'Le fichier "'+fn+'" a été créé dans le répertoire de travail.');
    end else
      FIni := TIniFile.Create( fpath + fn + '.ini' );

    FCfg := TStringList.Create;
    sect := TStringList.Create;

    FIni.ReadSections(sect);

    for i := 0 to sect.Count -1 do
    begin
      sectkvz := TStringList.Create;
      FIni.ReadSectionRaw(sect[i], sectkvz);
      for i2 := 0 to sectkvz.Count - 1 do
        FCfg.Add(sectkvz.Names[i2]+'='+self.UnescapeStr(sectkvz.ValueFromIndex[i2]));
      sectkvz.Free;
    end;

    formatters.ShortDateFormat := 'DD-MM-YYYY';
    formatters.ShortTimeFormat := 'hh:ii:ss';
    formatters.DateSeparator := '-';
    formatters.DecimalSeparator := '.';
    formatters.CurrencyDecimals := 2;

    except on e:Exception do
    begin
      ShowMessage(e.Message);
//      Application.Terminated := true;
    end;
  end;
end;

function TBaseFacturations.ConvToLocalDecSep(const inputStr: string): string;
begin
  if not Assigned(FRe) then
    FRe := TRegExpr.Create;
  FRe.Expression:='[,.]+';
  FRe.ModifierStr:='igs';
  result := FRe.Replace(inputStr,DecimalSeparator,true);
end;

function TBaseFacturations.SaveFileConfig(): boolean;
var
  i: integer;
begin
    try
      FIni.WriteString('main','pdfrep',FCfg.Values['pdfrep']);
      FIni.WriteString('main','dbpath',FCfg.Values['dbpath']);
      FIni.UpdateFile;
      except on e:Exception do
        Application.Terminate;
    end;
end;

function TBaseFacturations.SaveDBConfig(): boolean;
var
  i: integer;
  zqConf: TZQuery;
begin
    try
      zqConf := TZQuery.Create(nil);
      zqConf.Connection := ZConn;
      zqConf.SQL.Text := 'DELETE FROM config;';
      zqConf.ExecSQL;
      for i := 0 to FCfg.Count - 1 do
      begin
        if FCfg.Objects[i] = nil then continue;
        zqConf.SQL.Text := 'INSERT INTO config (nom,valeur,vt) VALUES (:Param3, :Param4, :Param2);';
        if FCfg.Objects[i] <> nil then
        begin
          zqConf.Params.ParamByName('Param3').AsString := FCfg.Names[i];
          zqConf.Params.ParamByName('Param4').AsString := FCfg.ValueFromIndex[i];
          zqConf.Params.ParamByName('Param2').AsInteger := Integer(TStrRowObj(FCfg.Objects[i]).vt);
          zqConf.ExecSQL;
        end;
      end;
      ZConn.Commit;
      zqConf.Close;
      zqConf.Free;
      except on e:Exception do
      begin
        zqConf.Free;
        Application.Terminate;
      end;
    end;
end;

procedure TBaseFacturations.SetCfgVal(key, value: string);
begin
  FCfg.Values[key] := value;
end;

procedure TBaseFacturations.SetCfgVal(key, value: string; ro: TStrRowObj);
var idx,i: integer;
  fro: TStrRowObj;
begin
  idx := FCfg.IndexOfName(key);
  //update on key not found with db id
  if (idx < 0) and (ro.id > 0) then
  begin
    for i := 0 to FCfg.Count - 1 do
    begin
      fro := TStrRowObj(FCfg.Objects[i]);
      if fro = nil then continue;
      if fro.id = ro.id then
      begin
        FCfg[i] := key+'='+value;
        idx := i;
        break;
      end;
    end;
  end else //update or create on new idx of key
  begin
    FCfg.Values[key] := value;
    idx := FCfg.IndexOfName(key);
  end;
  FCfg.Objects[idx] := ro;
end;

function TBaseFacturations.RemCfgVal(key: string; ro: TStrRowObj): boolean;
var i,idx: integer;
  fro: TStrRowObj;
begin
  result := true;
  idx := FCfg.IndexOfName(key);
  if (idx < 0) and (ro <> nil) and (ro.id > 0) then
  begin
    for i:=0 to FCfg.Count -1 do
    begin
      fro := TStrRowObj(FCfg.Objects[i]);
      if fro = nil then continue;
      if fro.id = ro.id then
      begin
        FCfg.Objects[i].Free;
        FCfg.Delete(i);
        result := true;
        break;
      end;
    end;
  end else if idx >= 0 then
  begin
    FCfg.Delete(idx);
    result := true;
  end;
end;

function TBaseFacturations.GetCfgVal(key: string): string;
begin
  if FCfg.IndexOfName(key) < 0 then
    result := ''
    else
    result := FCfg.Values[key];
end;

function TBaseFacturations.GetDBCfgVal(key: string): TVTRetType;
var i: integer;
begin
  i := FCfg.IndexOfName(key);
  if i < 0 then
  begin
    result.Value := '';
    result.robj := TStrRowObj.Create;
    result.robj.vt := TVTEnum.vtInvalid;
    result.robj.id := 0;
  end else
  begin
    result.Value := FCfg.Values[key];
    Result.robj := TStrRowObj(FCfg.Objects[i]);
    {*
    Result.robj.vt := TVtEnum(TStrRowObj(FCfg.Objects[i]).vt);
    result.robj.id := TStrRowObj(FCfg.Objects[i]).id;
    *}
  end;
end;

function TBaseFacturations.GetCfgValues(): TStringList;
begin
    result := FCfg;
end;

function TBaseFacturations.LastInsertID(const TableName: String): Integer;
begin
  Result := -1;
  with zqlid do
  begin
    SQL.Text := 'SELECT MAX(id) AS last_insert_id FROM :TableName';
    Params.ParamByName('TableName').AsString := TableName;
    Open;
    Result := FieldByName('last_insert_id').AsInteger;
    Close;
  end;
end;

function TBaseFacturations.MeasureText(Text: string; Width: LongInt): integer;
var
  i: integer;
  ch: char;
  tmpWidth: Extended;
  tmpTotalWidth: Extended;
  Prn: TPrinter;
begin
  Prn := Printer;
  Result := 0;
  tmpTotalWidth := 0;

  Text := UTF8Decode(Text);

  // calculate number of charactor contain in the specified width.
  for i := 1 to Length(Text) do
  begin
    ch := Text[i];
    tmpWidth := Prn.Canvas.Font.GetTextWidth(Text[i]) * Prn.Canvas.Font.Size / 1000;
{*
    if HorizontalScaling <> 100 then
      tmpWidth := tmpWidth * FHorizontalScaling / 100;
    if tmpWidth > 0 then
      tmpWidth := tmpWidth + FCharSpace
    else
      tmpWidth := 0;
*}
    if (ch = ' ') and (i <> Length(Text)) then
      tmpWidth := tmpWidth + 100;// FWordSpace;

    tmpTotalWidth := tmpTotalWidth + tmpWidth;
    if tmpTotalWidth > Width then
      Break;
    inc(Result);
  end;
end;



{******************************************************************************}


function FillListComponentFormat(var zq: TZReadOnlyQuery;var  wc:TWinControl; formatter: string): boolean;
var
  i: integer;
  iStr: string;
  cbVal: string;
  fd: TField;
  o:TIntObj;
begin
  result := false;
  try
    if wc.ClassType = TComboBox then
      TComboBox(wc).Items.Clear
    else if wc.ClassType = TListBox then
      TListBox(wc).Items.Clear;

    zq.Active := true;
    zq.ExecSQL;
    zq.Open;
    zq.First;
    while not zq.EOF do
    begin
      cbVal := formatter;
      for i := 1 to zq.Fields.Count do
      begin
        iStr := 'f'+IntToStr(i);
        fd := zq.FindField(iStr);
        if Assigned(fd) then
        begin
          cbVal := StringReplace(cbVal,'%s',zq.FieldByName(iStr).AsString, []);
        end;
        fd := nil;
      end;
      fd := zq.FindField('object');
      if wc.ClassType = TComboBox then
      begin
        if Assigned(fd) then
        begin
          o := TIntObj.Create();
          o.Value := zq.FieldByName('object').AsInteger;
          TComboBox(wc).AddItem(cbVal, o)
        end else
          TComboBox(wc).AddItem(cbVal, nil);
      end else if wc.ClassType = TListBox then
      begin
        if Assigned(fd) then
        begin
          o := TIntObj.Create();
          o.Value := zq.FieldByName('object').AsInteger;
          TListBox(wc).Items.AddObject(cbVal,o);
        end else
          TListBox(wc).Items.AddObject(cbVal, nil);
      end;
      zq.Next;
    end;
    result := true;
  finally
    zq.Close;
    zq.Active := false;
  end;
end;


{******************************************************************************}

end.

