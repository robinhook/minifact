unit UMenuPrincipal;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Dialogs, StdCtrls, Buttons;
  //UCommons, ZConnection;

type

  { TfrmMain }

  TfrmMain = class(TForm)
    btnImpots: TButton;
    btnJournal: TButton;
    btnClients: TButton;
    btnFactures: TButton;
    btnProduits: TButton;
    btnBilan: TButton;
    btConfigure: TButton;
    lbSoftInfo: TLabel;
    lblTitle: TLabel;
    SpeedButton1: TSpeedButton;
    procedure btConfigureClick(Sender: TObject);
    procedure btnBilanClick(Sender: TObject);
    procedure btnClientsClick(Sender: TObject);
    procedure btnFacturesClick(Sender: TObject);
    procedure btnImpotsClick(Sender: TObject);
    procedure btnJournalClick(Sender: TObject);
    procedure btnProduitsClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  frmMain: TfrmMain;

implementation

uses UConfigure, UClients, UProduits, UFactures, UBilan, UJournal, UImpots, UCommons, VersionSupport;

{$R *.lfm}

{ TfrmMain }

procedure TfrmMain.btnProduitsClick(Sender: TObject);
begin
  frmProduits.Visible:=true;
end;

procedure TfrmMain.FormClose(Sender: TObject; var CloseAction: TCloseAction);

begin
  try
    FreeAndNil(BaseFact);
  finally
    CloseAction := caFree;
  end;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  BaseFact := TBaseFacturations.Create;
  lbSoftInfo.Caption := 'Mini Facturateur v'+GetFileVersion()+#13#10
    +'Compilé le '+GetCompiledDate()+#13#10+'Pour '+GetTargetInfo();
end;

procedure TfrmMain.SpeedButton1Click(Sender: TObject);
var chmpath: string;
begin
  chmpath := BaseFact.AppPath+DirectorySeparator+'MiniFact.chm';
  {$IFDEF WINDOWS}
  ExecuteProcess(GetEnvironmentVariable('WINDIR')+'\hh.exe',['help.chm']);
  {$ELSE IFDEF LINUX}
  ExecuteProcess('/bin/xdg-open',['help.chm']);
  {$ENDIF}
end;

procedure TfrmMain.btnFacturesClick(Sender: TObject);
begin
  frmFactures.Visible:=true;
end;

procedure TfrmMain.btnImpotsClick(Sender: TObject);
begin
  frmImpots.Visible := true;
end;

procedure TfrmMain.btnJournalClick(Sender: TObject);
begin
  frmJournal.Visible := true;
end;

procedure TfrmMain.btnClientsClick(Sender: TObject);
begin
  frmClients.Visible:=true;
end;

procedure TfrmMain.btnBilanClick(Sender: TObject);
begin
  frmBilan.Visible := true;
end;

procedure TfrmMain.btConfigureClick(Sender: TObject);
begin
  frmConfigure.Visible := true;
end;

end.
