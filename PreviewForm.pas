unit PreviewForm;

{$mode objfpc}{$H+}

interface

uses Classes, SysUtils, Math, FileUtil, Forms, Controls, StdCtrls, ExtCtrls, ComCtrls, Graphics,
     Dialogs, Buttons, Printers, PrintersDlgs, PReport, UCommons;

const
      reportDPI = 1200;

  A4 = [210,297];
  RateWH = 210 / 297;

type
TProcApercu = procedure (Sender: TObject) of object;
PProcApercu = ^TProcApercu;

 { TPreviewForm }

TPreviewForm = class(TForm)
   pPage: TPanel;
   scbPage: TScrollBar;
   stbMain: TStatusBar;
   procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
   procedure FormCreate(Sender: TObject);
   procedure FormHide(Sender: TObject);
   procedure FormResize(Sender: TObject);
   procedure FormShow(Sender: TObject);
   procedure ToolButton3Click(Sender: TObject);
  private
   procedure PagePaint(Sender: TObject);
  public
   procApercu: TProcApercu;
end;

var
frmPreview: TPreviewForm;

implementation

{$R *.lfm}


function mm(val:double):longint;
begin
      Result:=round(val/25.4*reportDPI)
end;

{ TPreviewForm }

procedure TPreviewForm.PagePaint(Sender: TObject);
begin
  if Assigned(procApercu) then
    procApercu(pPage);
end;

procedure TPreviewForm.ToolButton3Click(Sender: TObject);
begin
end;

procedure TPreviewForm.FormCreate(Sender: TObject);
begin
  pPage.OnPaint := nil;
end;

procedure TPreviewForm.FormHide(Sender: TObject);
begin
  pPage.OnPaint := nil;
  procApercu := nil;
end;

procedure TPreviewForm.FormResize(Sender: TObject);
var ratio: single;
begin
  ratio := 21.9 / 29.7;
  if Height > Width / ratio then
    Width := ceil(Height * ratio);
  pPage.Height := Height - (stbMain.Height + 8);
  pPage.Width := Round(ratio * pPage.Height) - 8;
end;

procedure TPreviewForm.FormShow(Sender: TObject);
begin
  if Assigned(procApercu) then
    pPage.OnPaint := procApercu;
end;

procedure TPreviewForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  if CloseAction = caFree then
    procApercu := nil;
end;

end.