unit UListeProduits;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, StdCtrls, db, ZDataset;

type

  { TFrame2 }

  TFrame2 = class(TFrame)
    btAjouter: TButton;
    btUnEdit: TButton;
    edfChercher: TEdit;
    edQt: TEdit;
    Label1: TLabel;
    Label12: TLabel;
    Label3: TLabel;
    lbxListeProduits: TListBox;
    ZROQProduits: TZReadOnlyQuery;
    procedure btAjouterClick(Sender: TObject);
    procedure btUnEditClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    ProduitID, Quantite: Int64;
  end; 

implementation

{$R *.lfm}

uses UCommons;
{ TFrame2 }

procedure TFrame2.btUnEditClick(Sender: TObject);
begin
  ProduitID := -1;
  Visible := False;
end;

procedure TFrame2.btAjouterClick(Sender: TObject);
begin
  Quantite := StrToInt(edQt.Text);
  ProduitID := TIntObj(lbxListeProduits.Items.Objects[lbxListeProduits.ItemIndex]).Value;
  Visible := false;
end;

end.

