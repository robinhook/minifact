unit uframepaiements;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Grids, StdCtrls;

type

  { TFrame3 }

  TFrame3 = class(TFrame)
    btfValider: TButton;
    btfSupprimer: TButton;
    btFermer: TButton;
    cbfModePaiement: TComboBox;
    edfTxId: TEdit;
    edfMontant: TEdit;
    edfDate: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    sgPaiements: TStringGrid;
  private
    { private declarations }
  public
    { public declarations }
    procedure InitFrame();
  end;

implementation

uses strutils, UCommons;

{$R *.lfm}

procedure TFrame3.InitFrame();
var nmp:integer;
  mps:string;
begin
  sgPaiements.ColWidths[0] := 100;
  sgPaiements.ColWidths[1] := 90;
  sgPaiements.ColWidths[2] := 140;
  sgPaiements.ColWidths[3] := 0;
  sgPaiements.ColWidths[4] := 0;

  sgPaiements.Rows[0][0] := 'Mode';
  sgPaiements.Rows[0][1] := 'Montant';
  sgPaiements.Rows[0][2] := 'Date';
  sgPaiements.Rows[0][3] := '';
  sgPaiements.Rows[0][4] := '';

  cbfModePaiement.Items.Clear;
  nmp := 1;
  repeat
    mps := ExtractWord(nmp,BaseFact.GetCfgVal('modepaiement'),[',']);
    if mps <> '' then
      cbfModePaiement.Items.Add(mps)
    else break;
    inc(nmp);
  until mps = '';
end;

end.

