unit UJournal;

{$mode objfpc}

interface

uses
  Classes, SysUtils, FileUtil, DateUtils, math, db, Forms, Controls, Graphics, Dialogs, Grids,
  StdCtrls, ZDataset, ZConnection, UCommons;

type
  TColsNames = array[0..7] of string;
  { TfrmJournal }

  TfrmJournal = class(TForm)
    btValider: TButton;
    cbFilter: TComboBox;
    edDateDeb: TEdit;
    edDateFin: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    lbTotAcompte: TLabel;
    lbTotHT: TLabel;
    lbTotTTC: TLabel;
    lbTotRemise: TLabel;
    lbMontantImpayee: TLabel;
    sgLignesJournal: TStringGrid;
    zroJournal: TZReadOnlyQuery;
    procedure btValiderClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sgLignesJournalHeaderClick(Sender: TObject; IsColumn: Boolean;
      Index: Integer);
    procedure sgLignesJournalPrepareCanvas(sender: TObject; aCol,
      aRow: Integer; aState: TGridDrawState);
  private
    { private declarations }
    SQLParams:array of string;
    SQLQuery:array[0..1]of string;
    SQLColsNames:TColsNames;
    SortColName,OrderBy:string;
    Totals: TStringList;
    procedure RempliJournal(Sender:TObject);
    procedure CalculTotaux(Sender:TObject);
  public
    { public declarations }
  end;

var
  frmJournal: TfrmJournal;

implementation

{$R *.lfm}

{ TfrmJournal }

procedure TfrmJournal.FormCreate(Sender: TObject);
var cm,ca:integer;
  trim:integer;
  flds:TFields;
begin
  SortColName:='f.dt';
  OrderBy:='ASC';

  SQLColsNames[0] := 'f.dt';
  SQLColsNames[1] := 'lf.id';
  SQLColsNames[2] := 'lf.nom';
  SQLColsNames[3] := 'lf.qt';
  SQLColsNames[4] := 'lf.tva';
  SQLColsNames[5] := 'lf.prix';
  SQLColsNames[6] := 'lf.prix';
  SQLColsNames[7] := 'lf.remise';

  ca := YearOf(Now());
  cm := MonthOf(Now());
  trim := ceil(cm / 3);
  {*if(trim = 0) then
  begin
    ca := ca - 1;
    trim := 3;
    cm := trim*3;
  end else*} if(trim = 1) then
  begin
    cm := 1;
  end else begin
    cm := ((trim-1) * 3)+1;
  end;
  edDateDeb.Text := '01-'+Format('%.2d',[cm])+'-'+IntToStr(ca);
  edDateFin.Text := IntToStr(MonthDays[IsLeapYear(ca)][cm+2])+'-'+Format('%.2d',[cm+2])+'-'+IntToStr(ca);

  SetLength(SQLParams,5);
  SQLParams[0] := IntToStr(DateTimeToUnix(StrToDate(edDateDeb.Text,'dd-mm-YYYY','-')));
  SQLParams[1] := IntToStr(DateTimeToUnix(StrToDate(edDateFin.Text,'dd-mm-YYYY','-')));
  SQLParams[2] := IntToStr(integer(feDevis));
  SQLParams[3] := SortColName;
  SQLParams[4] := OrderBy;

  sgLignesJournal.ColWidths[0] := 88;
  sgLignesJournal.ColWidths[1] := 64;
  sgLignesJournal.ColWidths[2] := 360;
  sgLignesJournal.ColWidths[3] := 32;
  sgLignesJournal.ColWidths[4] := 48;
  sgLignesJournal.ColWidths[5] := 60;
  sgLignesJournal.ColWidths[6] := 60;
  sgLignesJournal.ColWidths[7] := 60;
  sgLignesJournal.ColWidths[8] := 0;
  sgLignesJournal.ColWidths[9] := 0;


  zroJournal.Connection := BaseFact.ZConn;

  SQLQuery[0] := 'SELECT f.id FROM factures f LEFT JOIN paiements p ON f.id = p.fid WHERE ((f.dt >= %s AND f.dt <= %s) OR (p.dt >= %s AND p.dt <= %s)) AND f.etat != %s GROUP BY f.id;';
  SQLQuery[1] := 'SELECT f.dt, lf.id, lf.fid, lf.nom, lf.qt, lf.tva, lf.prix, lf.remise, lf.`type`, f.etat FROM factures f INNER JOIN lignesfactures lf ON f.id = lf.fid WHERE f.id = %s AND f.etat != %s ORDER BY %s %s;';
  SQLQuery[2] := 'SELECT DISTINCT(type) AS types FROM lignesfactures ORDER BY type ASC;';

  SQLQuery[3] := 'SELECT CASE WHEN f.id IS NULL THEN p.fid ELSE f.id END, SUM(p.montant) AS acompte FROM factures f LEFT JOIN paiements p  ON f.id = p.fid WHERE ((f.dt >= %s AND f.dt <= %s) OR (p.dt >= %s AND p.dt <= %s)) AND f.etat != %s GROUP BY f.id;';
  SQLQuery[4] := 'SELECT fid, `type`, prix, ((prix/100)*tva), remise FROM lignesfactures WHERE fid = %s;';

  sgLignesJournal.Rows[0][0] := 'Date';
  sgLignesJournal.Rows[0][1] := 'FID-ID';
  sgLignesJournal.Rows[0][2] := 'Nom';
  sgLignesJournal.Rows[0][3] := 'Qt.';
  sgLignesJournal.Rows[0][4] := 'TVA';
  sgLignesJournal.Rows[0][5] := 'Prix HT';
  sgLignesJournal.Rows[0][6] := 'Prix TTC';
  sgLignesJournal.Rows[0][7] := 'Remise';
  sgLignesJournal.Rows[0][8] := '';
  sgLignesJournal.Rows[0][9] := '';

  zroJournal.SQL.Text := SQLQuery[2];
  zroJournal.ExecSQL;
  zroJournal.Open;
  zroJournal.First;
  while not zroJournal.EOF do
  begin
    flds := zroJournal.Fields;
    cbFilter.Items.Add(flds[0].AsString);
    zroJournal.Next;
  end;
  zroJournal.Close;

  btValider.Click;
end;

procedure TfrmJournal.sgLignesJournalHeaderClick(Sender: TObject;
  IsColumn: Boolean; Index: Integer);
var c:TStringGrid;
  colName:string;
begin
  //récup nom colonne
  if(not IsColumn) then exit;
  c := TStringGrid(Sender);
  //Index out of bounds !!!
  colName := SQLColsNames[Index];
  if(colName <> SortColName) then
    OrderBy:='ASC'
  else if OrderBy = 'ASC' then
    OrderBy := 'DESC'
  else OrderBy := 'ASC';
  SortColName := colName;
  btValiderClick(Sender);
end;

procedure TfrmJournal.sgLignesJournalPrepareCanvas(sender: TObject; aCol,
  aRow: Integer; aState: TGridDrawState);
begin
  if not (gdFixed in aState) then
    if not (gdSelected in aState) then
      if sgLignesJournal.Cells[8,aRow] = '0' then begin
        sgLignesJournal.Canvas.Brush.Color := clRed;
      end else begin
        if sgLignesJournal.Cells[9,aRow] = 'Service' then
          sgLignesJournal.Canvas.Brush.Color:=TColor($F0F0F0)
        else if sgLignesJournal.Cells[9,aRow] = 'Produit' then
          sgLignesJournal.Canvas.Brush.Color:=TColor($D0D0D0)
        else if sgLignesJournal.Cells[9,aRow] = 'FdP' then
          sgLignesJournal.Canvas.Brush.Color:=clGray;
      end;
end;

procedure TfrmJournal.btValiderClick(Sender: TObject);
begin
  sgLignesJournal.RowCount:=1;
  SetLength(SQLParams,5);
  SQLParams[0] := IntToStr(DateTimeToUnix(StrToDate(edDateDeb.Text,'dd-mm-YYYY','-')));
  SQLParams[1] := IntToStr(DateTimeToUnix(StrToDate(edDateFin.Text,'dd-mm-YYYY','-')));
  SQLParams[2] := IntToStr(integer(feDevis));
  SQLParams[3] := SortColName;
  SQLParams[4] := OrderBy;
  RempliJournal(Sender);
  CalculTotaux(Sender);
end;

procedure TfrmJournal.CalculTotaux(Sender:TObject);
var
  i,i2,i3:integer;
  totFactTTC, totAcompte, ratio: extended;
  filter: string;
  curJnl,curTotJnl: TTotJournal;
  flds: TFields;
  factJnl,totFactJnl: TStringList;
  zroLFact: TZReadOnlyQuery;
begin
  lbTotHT.Caption:='0';
  lbTotTTC.Caption:='0';
  lbTotRemise.Caption:='0';
  lbTotAcompte.Caption:='0';
  lbMontantImpayee.Caption:='0';

  zroLFact := TZReadOnlyQuery.Create(nil);
  zroLFact.Connection := BaseFact.ZConn;

  totFactJnl := TStringList.Create;
  for i := 1 to cbFilter.Items.Count - 1 do
  begin
    i2 := totFactJnl.Add(cbFilter.Items[i]);
    totFactJnl.Objects[i2] := TTotJournal.Create(cbFilter.Items[i]);
  end;

  filter := cbFilter.Items[cbFilter.ItemIndex];

  zroJournal.SQL.Text := Format(SQLQuery[3], [SQLParams[0],SQLParams[1],SQLParams[0],SQLParams[1],SQLParams[2]]);
  zroJournal.ExecSQL;
  zroJournal.Open;
  zroJournal.First;
  while not zroJournal.EOF do
  begin
    totFactTTC := 0;
{$IFDEF WINDOWS}
    totAcompte := StrToFloatDef(StringReplace(zroJournal.Fields[1].AsString,'.',',',[rfReplaceAll]),0.0);
{$ELSE IFDEF LINUX}
    totAcompte := StrToFloatDef(zroJournal.Fields[1].AsString,0.0);
{$ENDIF}
    factJnl := TStringList.Create;
    for i := 1 to cbFilter.Items.Count - 1 do
    begin
      i2 := factJnl.Add(cbFilter.Items[i]);
      factJnl.Objects[i2] := TTotJournal.Create(cbFilter.Items[i]);
    end;
    zroLFact.SQL.Text := Format(SQLQuery[4],[zroJournal.fields[0].AsString]);
    zroLFact.ExecSQL;
    zroLFact.Open;
    zroLFact.First;
    while not zroLFact.EOF do
    begin
      flds := zroLFact.Fields;
      i := factJnl.IndexOf(flds[1].AsString);
      curJnl := TTotJournal(factJnl.Objects[i]);
      //incrémenter les lignes de totaux et calculer le ttc
      curJnl.TotHT:=curJnl.TotHT + flds[2].AsFloat;
      curJnl.TotTTC:=curJnl.TotTTC + flds[2].AsFloat + flds[3].AsFloat;
      curJnl.TotTVA:=curJnl.TotTVA + flds[3].AsFloat;
      curJnl.TotRemise:=curJnl.TotRemise + flds[4].AsFloat;
      totFactTTC:=totFactTTC + flds[2].AsFloat + flds[3].AsFloat;
      zroLFact.Next;
    end;
    zroLFact.Close;
    //calculer les ratio sur les types
    for i := 1 to cbFilter.Items.Count - 1 do
    begin
      i2 := factJnl.IndexOf(cbFilter.Items[i]);
      curJnl := TTotJournal(factJnl.Objects[i2]);
      i3 := totFactJnl.IndexOf(cbFilter.Items[i]);
      curTotJnl := TTotJournal(totFactJnl.Objects[i3]);
      ratio := (curJnl.TotTTC - curJnl.TotRemise) / totFactTTC;
      curTotJnl.TotHT := curTotJnl.TotHT + curJnl.TotHT;
      curTotJnl.TotTTC := curTotJnl.TotTTC + curJnl.TotTTC;
      curTotJnl.TotAcompte := curTotJnl.TotAcompte + (totAcompte * ratio);
      curTotJnl.TotRemise := curTotJnl.TotRemise + curJnl.TotRemise;
    end;
    zroJournal.Next;
  end;
  for i := 0 to totFactJnl.Count - 1 do
  begin
    curTotJnl := TTotJournal(totFactJnl.Objects[i]);
    if filter = 'Tous' then
    begin
      lbTotHT.Caption:=Format('%.02f',[StrToFloatDef(lbTotHT.Caption,0.0)+curTotJnl.TotHT]);
      lbTotTTC.Caption:=Format('%.02f',[StrToFloatDef(lbTotTTC.Caption,0.0)+curTotJnl.TotTTC]);
      lbTotRemise.Caption:=Format('%.02f',[StrToFloatDef(lbTotRemise.Caption,0.0)+curTotJnl.TotRemise]);
      lbTotAcompte.Caption:=Format('%.02f',[StrToFloatDef(lbTotAcompte.Caption,0.0)+curTotJnl.TotAcompte]);
      lbMontantImpayee.Caption:=Format('%.02f',[StrToFloatDef(lbMontantImpayee.Caption,0.0)+(curTotJnl.TotTTC-curTotJnl.TotRemise-curTotJnl.TotAcompte)]);
    end else
    if filter = curTotJnl.tpLn then
    begin
      lbTotHT.Caption:=Format('%.02f',[curTotJnl.TotHT]);
      lbTotTTC.Caption:=Format('%.02f',[curTotJnl.TotTTC]);
      lbTotRemise.Caption:=Format('%.02f',[curTotJnl.TotRemise]);
      lbTotAcompte.Caption:=Format('%.02f',[curTotJnl.TotAcompte]);
      lbMontantImpayee.Caption:=Format('%.02f',[curTotJnl.TotTTC-curTotJnl.TotRemise-curTotJnl.TotAcompte]);
    end;
  end;
  zroJournal.Close;
end;

procedure TfrmJournal.RempliJournal(Sender:TObject);
var pht, pttc, acpt: Extended;
  i: integer;
  line: TStringList;
  flds: TFields;
  filter: string;
  zroFacts: TZReadOnlyQuery;
begin
  i := 1;
  filter := cbFilter.Items[cbFilter.ItemIndex];

  zroFacts := TZReadOnlyQuery.Create(nil);
  zroFacts.Connection := BaseFact.ZConn;
  zroFacts.SQL.Text := Format(SQLQuery[0], [SQLParams[0],SQLParams[1],SQLParams[0],SQLParams[1],SQLParams[2]]);
  zroFacts.ExecSQL;
  zroFacts.Open;
  zroFacts.First;
  while not zroFacts.EOF do
  begin
    zroJournal.SQL.Text := Format(SQLQuery[1], [zroFacts.Fields[0].AsString,SQLParams[2],SQLParams[3],SQLParams[4]]);
    zroJournal.ExecSQL;
    zroJournal.Open;
    zroJournal.First;
    while not zroJournal.EOF do
    begin
      flds := zroJournal.Fields;

      pht := flds[6].AsFloat;
      pttc := pht + ((pht / 100) * flds[5].AsFloat);

      if ((filter = 'Tous') or (filter = flds[8].AsString)) then
      begin
        line := TStringList.Create;
        line.Add(FormatDateTime('dd-mm-YYYY',UnixToDateTime(flds[0].AsLargeInt)));
        line.Add(flds[2].AsString+'-'+flds[1].AsString);
        line.Add(flds[3].AsString);
        line.Add(flds[4].AsString);
        line.Add(flds[5].AsString);

        line.Add(FloatToStr(RoundTo(pht,-2)));
        line.Add(FloatToStr(RoundTo(pttc,-2)));
        line.Add(flds[7].AsString);
        line.Add(flds[9].AsString);
        line.Add(flds[8].AsString);

        sgLignesJournal.RowCount := i + 1;
        sgLignesJournal.Rows[i] := line;
        inc(i);
      end;
      zroJournal.Next;
    end;
    zroJournal.Close;
    zroFacts.Next;
  end;
  zroFacts.Close;
end;

end.

