unit UClients;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Menus, KDBGrids, ZDataset, ZConnection, types, KGrids, KDialogs;

type

  { TfrmClients }

  TfrmClients = class(TForm)
    btnValider: TButton;
    btUnEdit: TButton;
    dtsClients: TDatasource;
    edPhone2: TEdit;
    edPhone: TEdit;
    edNom: TEdit;
    edPrenom: TEdit;
    edEMail: TEdit;
    kgClients: TKDBGrid;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    mmAdresse: TMemo;
    MenuItem1: TMenuItem;
    pmClients: TPopupMenu;
    ZROQClients: TZReadOnlyQuery;
    procedure btnValiderClick(Sender: TObject);
    procedure btUnEditClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure kgClientsDrawCell(Sender: TObject; ACol, ARow: Integer; R: TRect;
      State: TKGridDrawState);
    procedure kgClientsMouseClickCell(Sender: TObject; ACol, ARow: Integer);
    procedure kgClientsMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure MenuItem1Click(Sender: TObject);
  private
    { private declarations }
    currCol, currRow, RowID: Integer;
  public
    { public declarations }
  end; 

var
  frmClients: TfrmClients;

implementation

uses UCommons;

{$R *.lfm}

{ TfrmClients }

procedure TfrmClients.FormCreate(Sender: TObject);
begin
  kgClients.ColWidths[0] := 0;
  kgClients.ColWidths[1] := 64;
  kgClients.ColWidths[2] := 64;
  kgClients.ColWidths[3] := 0;
  kgClients.ColWidths[4] := 80;
  kgClients.ColWidths[5] := 0;
  kgClients.ColWidths[6] := 112;

  ZROQClients.Connection := BaseFact.ZConn;
end;

procedure TfrmClients.FormDeactivate(Sender: TObject);
begin
end;

procedure TfrmClients.FormHide(Sender: TObject);
begin
  ZROQClients.Active:=false;
end;

procedure TfrmClients.FormShow(Sender: TObject);
begin
  ZROQClients.Active:=true;

end;

procedure TfrmClients.btnValiderClick(Sender: TObject);
var
  sql: string;
begin
  if btnValider.Caption = 'Ajouter' then
  begin
    sql := 'INSERT INTO clients (nom,prenom,adresse,phone,phone2,email) VALUES ('
         + QuotedStr(edNom.Text) + ','
         + QuotedStr(edPrenom.Text) + ','
         + QuotedStr(mmAdresse.Text) + ','
         + QuotedStr(edPhone.Text) + ','
         + QuotedStr(edPhone2.Text) + ','
         + QuotedStr(edEMail.Text) + ');';
  end else
  begin
    sql := 'UPDATE clients SET '
         + 'nom = ' + QuotedStr(edNom.Text) + ','
         + 'prenom = ' + QuotedStr(edPrenom.Text) + ','
         + 'adresse = ' + QuotedStr(mmAdresse.Text) + ','
         + 'phone = ' + QuotedStr(edPhone.Text) + ','
         + 'phone2 = ' + QuotedStr(edPhone2.Text) + ','
         + 'email = ' + QuotedStr(edEMail.Text)
         + ' WHERE id = ' + QuotedStr(IntToStr(RowID)) + ';';

    btnValider.Caption := 'Ajouter';
    RowID := -1;
  end;
  if not BaseFact.ZConn.ExecuteDirect(sql) then
    ShowMessage('Opération échoué!');

  edNom.Clear;
  edPrenom.Clear;
  mmAdresse.Text := '';
  edPhone.Clear;
  edPhone2.Clear;
  edEMail.Clear;

  ZROQClients.Refresh;
end;

procedure TfrmClients.btUnEditClick(Sender: TObject);
begin
  btnValider.Caption := 'Ajouter';

  RowID := -1;
  edNom.Clear;
  edPrenom.Clear;
  mmAdresse.Text := '';
  edPhone.Clear;
  edPhone2.Clear;
  edEMail.Clear;

  btUnEdit.Visible := false;
end;

procedure TfrmClients.FormActivate(Sender: TObject);
begin
end;

procedure TfrmClients.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
end;

procedure TfrmClients.kgClientsDrawCell(Sender: TObject; ACol, ARow: Integer;
  R: TRect; State: TKGridDrawState);
begin
  TKCustomGrid(Sender).Cell[ACol, ARow].ApplyDrawProperties;
  if (aRow > 0) then
  begin
    if State * [gdFixed, gdSelected] = [] then
    begin
      if ARow mod 2 = 0 then
        TKCustomGrid(Sender).CellPainter.Canvas.Brush.Color := Color
      else
        TKCustomGrid(Sender).CellPainter.Canvas.Brush.Color := RGBToColor(220,220,255);
    end;
  end;
  TKCustomGrid(Sender).CellPainter.DefaultDraw;
end;

procedure TfrmClients.kgClientsMouseClickCell(Sender: TObject; ACol,
  ARow: Integer);
begin
  if currRow > kgClients.FixedRows - 1 then
  begin
    rowID := StrToInt(kgClients.Cells[0,currRow]);
    edNom.Text := kgClients.Cells[1,currRow];
    edPrenom.Text := kgClients.Cells[2,currRow];
    mmAdresse.Text := kgClients.Cells[3,currRow];
    edPhone.Text := kgClients.Cells[4,currRow];
    edPhone2.Text := kgClients.Cells[5,currRow];
    edEMail.Text := kgClients.Cells[6,currRow];
    btnValider.Caption := 'Modifier';
    btUnEdit.Visible := true;
  end;
end;

procedure TfrmClients.kgClientsMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  kgClients.MouseToCell(X,Y,currCol,currRow);
end;

procedure TfrmClients.MenuItem1Click(Sender: TObject);
var
  sql: string;
begin
  if currRow > kgClients.FixedRows - 1 then
  begin
    sql := 'DELETE FROM clients WHERE id = ' + QuotedStr(kgClients.Cells[0,currRow]);
    if not BaseFact.ZConn.ExecuteDirect(sql) then
      ShowMessage('Echec suppression!');
    ZROQClients.Refresh;
  end;
end;

end.

