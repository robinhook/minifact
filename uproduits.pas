unit UProduits;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Menus, KDBGrids, ZDataset, KGrids, types;

type

  { TfrmProduits }

  TfrmProduits = class(TForm)
    btValider: TButton;
    btUnEdit: TButton;
    cbTVA: TComboBox;
    cbType: TComboBox;
    dtsProduits: TDatasource;
    edPrixHT: TEdit;
    edNomProd: TEdit;
    kgProduits: TKDBGrid;
    lbNomProd1: TLabel;
    lbPrixHT: TLabel;
    lbTVA: TLabel;
    lbNomProd: TLabel;
    MenuItem1: TMenuItem;
    pmProduits: TPopupMenu;
    zqProduits: TZQuery;
    procedure btUnEditClick(Sender: TObject);
    procedure btValiderClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure kgProduitsDrawCell(Sender: TObject; ACol, ARow: Integer;
      R: TRect; State: TKGridDrawState);
    procedure kgProduitsMouseClickCell(Sender: TObject; ACol, ARow: Integer);
    procedure MenuItem1Click(Sender: TObject);
    procedure pmProduitsPopup(Sender: TObject);
  private
    { private declarations }
    rowID: Integer;
    slTaux: TStringList;
    function ItmIdxFromTVAID(tvaid: integer): integer;
    procedure setCellFromMouse(ControlCoord: TPoint);
  public
    { public declarations }
  end; 

var
  frmProduits: TfrmProduits;

implementation

uses UCommons;

{$R *.lfm}

{ TfrmProduits }

procedure TfrmProduits.FormCreate(Sender: TObject);
var
  zq: TZQuery;
  o: TIntObj;
begin

  kgProduits.ColWidths[0] := 24;
  kgProduits.ColWidths[1] := 64;
  kgProduits.ColWidths[2] := 320;
  kgProduits.ColWidths[3] := 48;
  kgProduits.ColWidths[4] := 96;

  zqProduits.Connection := BaseFact.ZConn;

  slTaux := TStringList.Create;

  zq := TZQuery.Create(nil);
  zq.Connection := BaseFact.ZConn;
  zq.SQL.Text := 'SELECT id, taux FROM tvas ORDER BY taux ASC';
  zq.ExecSQL;
  zq.Open;
//  zq.First;
  while not zq.EOF do
  begin
    o := TIntObj.Create();
    o.Value := zq.Fields[0].AsInteger;
    slTaux.AddObject(zq.Fields[1].AsString, o);
    //slTaux.Add(zq.Fields[1].AsString);
    zq.Next;
  end;
  zq.Close;
  zq.Free;

  cbTVA.Items.AddStrings(slTaux);
end;

procedure TfrmProduits.FormDestroy(Sender: TObject);
begin
end;

procedure TfrmProduits.btValiderClick(Sender: TObject);
var
  sql: string;
  tvaid: integer;
begin
  tvaid := TIntObj(cbTVA.Items.Objects[cbTVA.ItemIndex]).Value;

  if btValider.Caption = 'Ajouter' then
  begin
    sql := 'INSERT INTO produits (nom,tvaid,prix,`type`) VALUES('
       + QuotedStr(edNomProd.Text)+','
       + QuotedStr(IntToStr(tvaid))+','
       + QuotedStr(StringReplace(edPrixHT.Text,',','.', []))+','
       + QuotedStr(cbType.Items[cbType.ItemIndex])+');';

    if not BaseFact.ZConn.ExecuteDirect(sql) then
      ShowMessage('Echec insertion!');
  end else
  begin
    sql := 'UPDATE produits SET nom = ' + QuotedStr(edNomProd.Text)
          +', tvaid = ' + QuotedStr(IntToStr(tvaid))
          +', prix = ' + QuotedStr(StringReplace(edPrixHT.Text,',','.', []))
          +', `type` = ' + QuotedStr(cbType.Items[cbType.ItemIndex])
          +' WHERE id = ' + QuotedStr(IntToStr(rowID))+';';
    if not BaseFact.ZConn.ExecuteDirect(sql) then
      ShowMessage('Echec insertion!');
    rowID := -1;
    edNomProd.Text := '';
    cbTVA.ItemIndex := 0;
    cbType.ItemIndex := 0;
    edPrixHT.Text := '0,0';
    btValider.Caption := 'Ajouter';
    btUnEdit.Visible := false;
  end;
  zqProduits.Refresh;
end;

procedure TfrmProduits.btUnEditClick(Sender: TObject);
begin
  rowID := -1;
  edNomProd.Text := '';
  cbTVA.ItemIndex := 0;
  edPrixHT.Text := '0,0';
  cbType.ItemIndex := 0;
  btValider.Caption := 'Ajouter';
  btUnEdit.Visible := false;
end;

procedure TfrmProduits.FormHide(Sender: TObject);
begin
  zqProduits.Active:=false;
end;

procedure TfrmProduits.FormShow(Sender: TObject);
begin
  zqProduits.Active:=true;
end;

procedure TfrmProduits.kgProduitsDrawCell(Sender: TObject; ACol, ARow: Integer;
  R: TRect; State: TKGridDrawState);
begin
  TKCustomGrid(Sender).Cell[ACol, ARow].ApplyDrawProperties;
  if (aRow > 0) then
  begin
    if State * [gdFixed, gdSelected] = [] then
    begin
      if ARow mod 2 = 0 then
        TKCustomGrid(Sender).CellPainter.Canvas.Brush.Color := Color
      else
        TKCustomGrid(Sender).CellPainter.Canvas.Brush.Color := RGBToColor(220,220,255);
    end;
  end;
  TKCustomGrid(Sender).CellPainter.DefaultDraw;
end;

procedure TfrmProduits.kgProduitsMouseClickCell(Sender: TObject; ACol,
  ARow: Integer);
begin
  if kgProduits.Row > kgProduits.FixedRows - 1 then
  begin
    rowID := StrToInt(kgProduits.Cells[0,kgProduits.Row]);
    cbType.ItemIndex := cbType.Items.IndexOf(kgProduits.Cells[1,kgProduits.Row]);
    edNomProd.Text := kgProduits.Cells[2,kgProduits.Row];
    cbTVA.ItemIndex := ItmIdxFromTVAID(StrToint(kgProduits.Cells[3,kgProduits.Row]));
    edPrixHT.Text := kgProduits.Cells[4,kgProduits.Row];
    btValider.Caption := 'Modifier';
    btUnEdit.Visible := true;
  end;
end;

procedure TfrmProduits.MenuItem1Click(Sender: TObject);
var
  sql: string;
begin
  if kgProduits.Row > kgProduits.FixedRows - 1 then
  begin
    sql := 'DELETE FROM produits WHERE id = ' + QuotedStr(kgProduits.Cells[0,kgProduits.Row]);
    if not BaseFact.ZConn.ExecuteDirect(sql) then
      ShowMessage('Echec suppression!');
    zqProduits.Refresh;
  end;
end;

procedure TfrmProduits.setCellFromMouse(ControlCoord: TPoint);
var
  NewCell: TPoint;
begin
  ControlCoord := kgProduits.ScreenToControl(pmProduits.PopupPoint);
  if kgProduits.MouseToCell(ControlCoord.X,ControlCoord.Y,NewCell.X,NewCell.Y) then
  begin
    kgProduits.Row := NewCell.Y;
    kgProduits.Col := NewCell.X;
  end;
end;

procedure TfrmProduits.pmProduitsPopup(Sender: TObject);
begin
  setCellFromMouse(pmProduits.PopupPoint);
end;

function TfrmProduits.ItmIdxFromTVAID(tvaid: integer): integer;
var i: integer;
begin
  result := -1;
  for i := 0 to cbTVA.Items.Count - 1 do
    if TIntObj(cbTVA.Items.Objects[i]).Value = tvaid then
    begin
      result := i;
      Exit;
    end;
end;

end.

