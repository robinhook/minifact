unit UFactures;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, math, dateutils, db, FileUtil, LazFileUtils, Forms, Controls, Graphics,
  Dialogs, StdCtrls, ExtCtrls, DbCtrls, Grids, Menus, UListeProduits,
  UFrameFactures, PreviewForm, KDBGrids, PdfTypes, PdfDoc, ZDataset, ZConnection,
  PrintersDlgs, Printers, uRegExpr, UCommons, uframepaiements;

type

  { TfrmFactures }

  TfrmFactures = class(TForm)
    btValider: TButton;
    btGerer: TButton;
    cbClients: TComboBox;
    cbEtat: TComboBox;
    cbfModePaiement: TComboBox;
    edAcompte: TEdit;
    edfChercher: TEdit;
    edDate: TEdit;
    edRemise: TEdit;
    edSommeDu: TEdit;
    edfDate: TEdit;
    edNom: TEdit;
    edPrenom: TEdit;
    edTotHT: TEdit;
    edTotTTC: TEdit;
    frmListeFactures: TFrame1;
    frmPaiements: TFrame3;
    frmListeProduits: TFrame2;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label12: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    lbAcompte: TLabel;
    lbSommeDu: TLabel;
    lbInfosSociete: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    lbNomSociete: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    MainMenu1: TMainMenu;
    MenuItem1: TMenuItem;
    miRemise: TMenuItem;
    miPrix: TMenuItem;
    miLFLibelle: TMenuItem;
    miImprimante: TMenuItem;
    miImpression: TMenuItem;
    miFichierPdf: TMenuItem;
    miSupprimer: TMenuItem;
    miFactures: TMenuItem;
    miNouvelle: TMenuItem;
    miEditer: TMenuItem;
    miLFSupprimer: TMenuItem;
    miLFAjouter: TMenuItem;
    mmAdresse: TMemo;
    pmLignesFact: TPopupMenu;
    pdFacture: TPrintDialog;
    sgLignesFact: TStringGrid;
    ZConnection1: TZConnection;
    ZQLignesFact: TZQuery;
    ZROQClients: TZReadOnlyQuery;
    procedure btEditerClick(Sender: TObject);
    procedure btFermerClick(Sender: TObject);
    procedure btfSupprimerClick(Sender: TObject);
    procedure btfValiderClick(Sender: TObject);
    procedure btGererClick(Sender: TObject);
    procedure btValiderClick(Sender: TObject);
    procedure cbClientsChange(Sender: TObject);
    procedure cbClientsKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState
      );
    procedure cbEtatChange(Sender: TObject);
    procedure edAcompteChange(Sender: TObject);
    procedure edfLFChercherChange(Sender: TObject);
    procedure edfLPChercherChange(Sender: TObject);
    procedure edRemiseChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure miEditerClick(Sender: TObject);
    procedure miFichierPdfClick(Sender: TObject);
    procedure miIprimerClick(Sender: TObject);
    procedure miLFAjouterClick(Sender: TObject);
    procedure miLFLibelleClick(Sender: TObject);
    procedure miLFSupprimerClick(Sender: TObject);
    procedure miNouvelleClick(Sender: TObject);
    procedure miLFPrixClick(Sender: TObject);
    procedure miLFRemiseClick(Sender: TObject);
    procedure miSupprimerClick(Sender: TObject);
    procedure pmLignesFactPopup(Sender: TObject);
    procedure sgPaiementsSelection(Sender: TObject; aCol, aRow: Integer);
  private
    { private declarations }
    TotHT, TotTTC, TotRemise: Extended;
    FLine: Integer;
    PdfDoc: TPDFDoc;
    procedure EditerFactures(fid: integer);

    procedure TextOut(X, Y: Single; S: string);
    procedure DrawLine(X1, Y1, X2, Y2, W: Single);
    procedure GenerePDF(Filename: String);
    procedure RecalcChamps(Sender: TObject);
  public
    { public declarations }
    procedure ImprimeFacture();
  end;

var
  frmFactures: TfrmFactures;

implementation

{$R *.lfm}

{ TfrmFactures }

procedure TfrmFactures.FormCreate(Sender: TObject);
begin
  sgLignesFact.ColWidths[0] := 32;
  sgLignesFact.ColWidths[1] := 80;
  sgLignesFact.ColWidths[2] := 380;
  sgLignesFact.ColWidths[3] := 64;
  sgLignesFact.ColWidths[4] := 48;
  sgLignesFact.ColWidths[5] := 60;
  sgLignesFact.ColWidths[6] := 60;
  sgLignesFact.ColWidths[7] := 60;

  sgLignesFact.Rows[0][0] := 'ID';
  sgLignesFact.Rows[0][1] := 'Type';
  sgLignesFact.Rows[0][2] := 'Nom';
  sgLignesFact.Rows[0][3] := 'Quantité';
  sgLignesFact.Rows[0][4] := 'TVA';
  sgLignesFact.Rows[0][5] := 'Prix HT';
  sgLignesFact.Rows[0][6] := 'Prix TTC';
  sgLignesFact.Rows[0][7] := 'Remise';

  lbNomSociete.Caption := BaseFact.GetCfgVal('nom');

  lbInfosSociete.Caption := BaseFact.GetCfgVal('adresse')+#13#10
                        +BaseFact.GetCfgVal('cp')+' '+BaseFact.GetCfgVal('ville')+#13#10#13#10
                        +'Tél.: '+BaseFact.GetCfgVal('tel')+#13#10
                        +'E-Mail: '+BaseFact.GetCfgVal('email')+#13#10#13#10
                        +'SIRET: '+BaseFact.GetCfgVal('siret');

  ZROQClients.Connection := BaseFact.ZConn;
  frmListeProduits.ZROQProduits.Connection := BaseFact.ZConn;
  ZQLignesFact.Connection := BaseFact.ZConn;
  frmListeFactures.ZROQFactures.Connection := BaseFact.ZConn;

  frmPaiements.InitFrame();
  frmPaiements.edfDate.Text := FormatDateTime('dd-mm-YYYY',Now(),BaseFact.formatters);
end;

procedure TfrmFactures.cbClientsChange(Sender: TObject);
var
  zq: TZReadOnlyQuery;
  IdCli,NameCli: string;
begin
  if cbClients.ItemIndex < 0 then Exit;
  zq := TZReadOnlyQuery.Create(nil);
  zq.Connection := BaseFact.ZConn;

  //cbClients.Items.GetNameValue(cbClients.ItemIndex,IdCli,NameCli);
  IdCli := IntToStr(TIntObj(cbClients.Items.Objects[cbClients.ItemIndex]).Value);

  zq.SQL.Text := 'SELECT nom,prenom,adresse FROM clients WHERE id = '+IdCli+' LIMIT 1;';
  zq.ExecSQL;
  zq.Open;
  zq.First;
  while not zq.EOF do
  begin
    edNom.Text := zq.Fields[0].AsString;
    edPrenom.Text := zq.Fields[1].AsString;
    mmAdresse.Text := zq.Fields[2].AsString;
    frmListeProduits.btAjouter.Enabled := true;
    zq.Next;
  end;
  zq.Close;
  zq.Free;
end;


procedure TfrmFactures.cbClientsKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var cb: TComboBox;
  str: string;
  i: integer;
begin
  if Pos(WideChar(Key),#0#8#0#13#0#10#0#27#0#35#0#36#0#37#0#39) > 0 then Exit;
  cb := TComboBox(Sender);
  str := cb.Text;
  if Length(str) < 2 then Exit;
  cb.Items.Clear;
{* new code make request to fill combobox *}
  ZROQClients.SQL.Text := 'SELECT id AS object, nom AS f1, prenom AS f2 FROM clients WHERE nom LIKE '+ QuotedStr('%'+str+'%') +' OR prenom LIKE '+ QuotedStr('%'+str+'%') +' OR adresse LIKE '+QuotedStr('%'+str+'%')+' OR email LIKE '+QuotedStr('%'+str+'%')+';';
  FillListComponentFormat(ZROQClients,TWinControl(cb),'%s %s');
  cb.DroppedDown:=true;
{* Old code that seeking full combobox
  for i:=0 to cb.Items.Count - 1 do
  begin
    if Pos(str,cb.Items[i]) > 0 then
    begin
      cb.ItemIndex := i;
      cb.DroppedDown := true;
      Exit;
    end;
  end;
*}
end;

procedure TfrmFactures.cbEtatChange(Sender: TObject);
begin
{*
  if cbEtat.ItemIndex = Integer(fePayee) then
   edfDate.Text := FormatDateTime('dd-mm-YYYY',Now());
*}
end;

procedure TfrmFactures.RecalcChamps(Sender: TObject);
var i: integer;
  pht,pttc,remise:Extended;
begin
  TotHT:=0;
  TotTTC:=0;
  TotRemise:=0;
  for i:=1 to sgLignesFact.RowCount -1 do
  begin
      remise := StrToFloatDef(sgLignesFact.Rows[i][7],0);
      pht := StrToFloatDef(sgLignesFact.Rows[i][5],0);
      pttc := pht + ((pht / 100) * StrToFloatDef(sgLignesFact.Rows[i][4],0));

      TotHT := TotHT + pht;
      TotTTC := TotTTC + pttc;
      TotRemise := TotRemise + remise;
  end;
end;

procedure TfrmFactures.edAcompteChange(Sender: TObject);
begin
  edRemise.Text := FloatToStr(RoundTo(TotRemise,-2));
  edTotHT.Text := FloatToStr(RoundTo(TotHT,-2));
  edTotTTC.Text := FloatToStr(RoundTo(TotTTC,-2));

  edSommeDu.Text := FloatToStr(RoundTo(TotTTC - (StrToFloatDef(edRemise.Text,0.0)+StrToFloatDef(edAcompte.Text,0.0)),-2));

  if cbEtat.ItemIndex <> LongInt(feDevis)  then
   begin
    if RoundTo(TotTTC,-2) = RoundTo(TotRemise+StrToFloatDef(edAcompte.Text,0.0),-2) then
      cbEtat.ItemIndex := LongInt(fePayee)
    else if TotTTC > RoundTo(TotRemise+StrToFloatDef(edAcompte.Text,0.0),-2) then
      cbEtat.ItemIndex := LongInt(feEncours)
    else cbEtat.ItemIndex := LongInt(feImpayee);
    cbEtatChange(Sender);
   end;
end;

procedure TfrmFactures.edfLFChercherChange(Sender: TObject);
var sqlQuery,str,inp: string;
  zqr:TZReadOnlyQuery;
begin
  if Length(frmListeFactures.edfChercher.Text) > 1 then
  begin
    str := frmListeFactures.edfChercher.Text;
    zqr := TZReadOnlyQuery.Create(nil);
    zqr.Connection := BaseFact.ZConn;
    zqr.SQL.Text := 'SELECT fid FROM paiements WHERE '
                   +' txid LIKE "%'+str+'%" OR STRFTIME("%d-%m-%Y",dt,"unixepoch") LIKE "%'+str+'%"';
    zqr.ExecSQL();
    zqr.Open();
    while not zqr.EOF do
    begin
      if inp = '' then
        inp := zqr.Fields[0].AsString
        else inp := inp+','+zqr.Fields[0].AsString;

      zqr.Next();
    end;
    zqr.Close;

    zqr.SQL.Text := 'SELECT f.id AS object,f.id AS f1, f.nom AS f2, f.prenom AS f3 FROM factures AS f JOIN lignesfactures AS lf ON f.id = lf.fid WHERE '
                   +'f.nom LIKE "%'+str+'%" OR f.prenom LIKE "%'+str+'%" OR f.adresse LIKE "%'+str+'%"'
                   +' OR STRFTIME("%d-%m-%Y",f.dt,"unixepoch") LIKE "%'+str+'%"'
                   +' OR f.id IN ('+inp+') OR lf.nom LIKE "%'+str+'%" OR lf.`type` LIKE "%'+str+'%" GROUP BY f.id';
    FillListComponentFormat(zqr,TWinControl(frmListeFactures.lbxListeFactures),'%s - %s %s');
  end else
    FillListComponentFormat(frmListeFactures.ZROQFactures,TWinControl(frmListeFactures.lbxListeFactures),'%s - %s %s');;
end;

procedure TfrmFactures.edfLPChercherChange(Sender: TObject);
var sqlQuery,str: string;
  zqr:TZReadOnlyQuery;
begin
  if Length(frmListeProduits.edfChercher.Text) > 1 then
  begin
    str := frmListeProduits.edfChercher.Text;
    zqr := TZReadOnlyQuery.Create(nil);
    zqr.Connection := BaseFact.ZConn;
    zqr.SQL.Text := 'SELECT id AS object, `type` AS f1, nom AS f2, prix AS f3 FROM produits WHERE '
                   +'nom LIKE "%'+str+'%" OR prix LIKE "%'+str+'%" OR `type` LIKE "%'+str+'%";';
    FillListComponentFormat(zqr,TWinControl(frmListeProduits.lbxListeProduits),'[%s] %s €%s');
  end else
    FillListComponentFormat(frmListeProduits.ZROQProduits,TWinControl(frmListeProduits.lbxListeProduits),'%s - %s %s');;
end;

procedure TfrmFactures.edRemiseChange(Sender: TObject);
begin
  edTotHT.Text := FloatToStr(RoundTo(TotHT,-2));
  edTotTTC.Text := FloatToStr(RoundTo(TotTTC,-2));
  edRemise.Text := FloatToStr(RoundTo(TotRemise,-2));
  edSommeDu.Text := FloatToStr(RoundTo(TotTTC - (TotRemise+StrToFloatDef(edAcompte.Text,0.0)),-2));
end;

procedure TfrmFactures.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  frmPaiements.Visible := false;
  frmListeFactures.Visible := false;
  frmListeProduits.Visible := false;
end;

procedure TfrmFactures.EditerFactures(fid: integer);
var
  pht, pttc, pacpt: Extended;
  i: integer;
  line: TStringList;
  flds: Tfields;
  zqc,zqd: TZReadOnlyQuery;
  n,p:string;
begin
    zqc := TZReadOnlyQuery.Create(nil);
    zqc.Connection := BaseFact.ZConn;
    zqc.SQL.Text := 'SELECT etat, nom, prenom, adresse, dt FROM factures WHERE id = '+IntToStr(fid)+';';
    zqc.ExecSQL;
    zqc.Open;
    zqc.First;
    while not zqc.EOF do
    begin
      zqd := TZReadOnlyQuery.Create(nil);
      zqd.Connection := BaseFact.ZConn;
      zqd.SQL.Text := 'SELECT modepment,montant,dt,txid,id FROM paiements WHERE fid = '+IntToStr(fid)+';';
      zqd.ExecSQL;
      zqd.Open;
      zqd.First;

      frmPaiements.sgPaiements.RowCount := zqd.RecordCount + 1;
      frmPaiements.sgPaiements.Row := 1;
      pacpt := 0;
      while not zqd.EOF do
      begin
        pacpt := pacpt + zqd.Fields[1].AsFloat;

        flds := zqd.Fields;
        line := TStringList.Create;
        line.Add(zqd.Fields[0].AsString);
        line.Add(zqd.Fields[1].AsString);
        line.Add(FormatDateTime('dd-mm-YYYY',UnixToDateTime(zqd.Fields[2].AsInteger)));
        line.Add(zqd.Fields[3].AsString);
        line.Add(zqd.Fields[4].AsString);

        frmPaiements.sgPaiements.Rows[frmPaiements.sgPaiements.Row] := line;
        frmPaiements.sgPaiements.Row := frmPaiements.sgPaiements.Row +1;

        zqd.Next;
      end;
      zqd.Close;
      zqd.Free;

      n := zqc.Fields[1].AsString;
      if(n = '-') then
        n := '';
      p := zqc.Fields[2].AsString;
      if(p = '-') then
        p := '';
      if(n <> '') xor (p <> '')then
        i := cbClients.Items.IndexOf(n+' '+p)
        else i := -1;
      if i = -1 then
      begin
        edNom.Text := zqc.Fields[1].AsString;
        edPrenom.Text := zqc.Fields[2].AsString;
        edAcompte.Text := FloatToStr(RoundTo(pacpt,-2));
      end else
      begin
        cbClients.ItemIndex := i;
        cbClients.OnChange(Self);
        edAcompte.Text := FloatToStr(RoundTo(pacpt,-2));
      end;
      mmAdresse.Lines.Text := zqc.Fields[3].AsString;
      cbEtat.ItemIndex := zqc.Fields[0].AsInteger;
      edDate.Text := FormatDateTime('dd-mm-YYYY',UnixToDateTime(zqc.Fields[4].AsInteger));
      zqc.Next;
    end;
    zqc.Close;
    zqc.Free;

    ZQLignesFact.SQL.Text := 'SELECT id, `type`, nom, qt, tva, prix, remise FROM lignesfactures WHERE fid = '+IntToStr(fid)+';';
    ZQLignesFact.ExecSQL;
    ZQLignesFact.Open;
    ZQLignesFact.First;
    TotHT := 0;
    TotTTC := 0;
    TotRemise:=0;
    i := 1;
    while not ZQLignesFact.EOF do
    begin
      flds := ZQLignesFact.Fields;
      line := TStringList.Create;
      line.Add(flds[0].AsString);
      line.Add(flds[1].AsString);
      line.Add(flds[2].AsString);
      line.Add(flds[3].AsString);
      line.Add(flds[4].AsString);

      pht := flds[5].AsFloat;
      pttc := pht + ((pht / 100) * flds[4].AsFloat);

      TotHT := totht + pht;
      TotTTC := totttc + pttc;
      TotRemise := TotRemise + flds[6].AsFloat;

      line.Add(FloatToStr(RoundTo(pht,-2)));
      line.Add(FloatToStr(RoundTo(pttc,-2)));
      line.Add(flds[6].AsString);

      sgLignesFact.RowCount := i + 1;
      sgLignesFact.Rows[i] := line;
      ZQLignesFact.Next;
      inc(i);
    end;
    ZQLignesFact.Close;
    edTotHT.Text := FloatToStr(RoundTo(TotHT,-2));
    edTotTTC.Text := FloatToStr(RoundTo(TotTTC,-2));
    edRemise.Text := FloatToStr(RoundTo(totremise,-2));
    edSommeDu.Text := FloatToStr(RoundTo(TotTTC - (StrToFloatDef(edRemise.Text,0.0)+StrToFloatDef(edAcompte.Text,0.0)),-2));
end;

procedure TfrmFactures.FormHide(Sender: TObject);
begin
//  ZROQClients.Active := false;
  ZQLignesFact.Active := false;
  frmListeFactures.Visible := false;
  frmListeProduits.Visible := false;
//  ZROQFactures.Active := false;
//  ZROQProduits.Active := false;
end;

procedure TfrmFactures.FormShow(Sender: TObject);
begin
//  ZROQClients.Active := true;
  ZQLignesFact.Active := true;
//  ZROQFactures.Active := true;
//  ZROQProduits.Active := true;

  FillListComponentFormat(ZROQClients,TWinControl(cbClients),'%s %s');
  FillListComponentFormat(frmListeProduits.ZROQProduits,TWinControl(frmListeProduits.lbxListeProduits),'[%s] %s %s€');
  FillListComponentFormat(frmListeFactures.ZROQFactures,TWinControl(frmListeFactures.lbxListeFactures),'%s - %s');

end;

procedure TfrmFactures.miEditerClick(Sender: TObject);
begin
  TotHT := 0;
  TotTTC := 0;
  FillListComponentFormat(frmListeFactures.ZROQFactures,TWinControl(frmListeFactures.lbxListeFactures),'[%s] %s %s');

  frmPaiements.Visible := false;
  frmListeFactures.Visible := true;
  frmListeProduits.Visible := false;
  GroupBox2.Visible := false;

  frmListeFactures.Visible := true;
  while frmListeFactures.Visible do
    Application.ProcessMessages;
  if frmListeFactures.FactureID > 0 then
  begin
    EditerFactures(frmListeFactures.FactureID);
    frmListeProduits.btAjouter.Enabled:=true;
  end;
  GroupBox2.Visible := true;
end;

procedure TfrmFactures.miFichierPdfClick(Sender: TObject);
var
  dt: TDateTime;
  c:char;
  filename: string;
begin
  if frmListeFactures.FactureID > 0 then
  begin
    if cbEtat.ItemIndex = LongInt(feDevis) then
      c := 'D' else c := 'F';
    if TryStrToDate(edDate.Text,dt,'DD-MM-YYYY',BaseFact.formatters.DateSeparator) then
    begin
      filename := BaseFact.GetCfgVal('pdfrep')+DirectorySeparator+c+Format('%.4d',[frmListeFactures.FactureID])+'D'+FormatDateTime('DDMMYY',dt)+'.pdf';
      GenerePDF(ExpandFileNameUTF8(filename));
    end else
      ShowMessage('La facture n''a pas de date valide!');
  end else
    ShowMessage('Veuillez éditer/valider une facture d''abord!');
end;

//right 4800 bottom 6826
procedure TfrmFactures.miIprimerClick(Sender: TObject);
var
  Prn: TPrinter;
begin
  Prn := Printer;
  if pdFacture.Execute then
  begin
    Prn := Printer;

    Prn.Title := 'Facture_'+IntToStr(frmListeFactures.FactureID);
    Prn.BeginDoc;

    ImprimeFacture();

    Prn.EndDoc;
  end;
end;

procedure TfrmFactures.miLFAjouterClick(Sender: TObject);
var
  pht, pttc: extended;
  sql: string;
  zq: TZReadOnlyQuery;
begin
  frmListeProduits.Visible := true;
  frmListeFactures.Visible := false;
  frmPaiements.Visible := false;
  GroupBox2.Visible := false;
  while frmListeProduits.Visible do
    Application.ProcessMessages;
  GroupBox2.Visible := true;

  if frmListeProduits.ProduitID > 0 then
  begin
    zq := TZReadOnlyQuery.Create(nil);
    zq.Connection := BaseFact.ZConn;
    zq.SQL.Text := 'SELECT p.`type`, p.nom, t.taux, p.prix FROM produits p INNER JOIN tvas t ON (p.tvaid = t.id) WHERE (p.id = '+QuotedStr(IntToStr(frmListeProduits.ProduitID))+');';
    zq.Open;

    sgLignesFact.RowCount := sgLignesFact.RowCount + 1;

    sgLignesFact.Rows[sgLignesFact.RowCount-1][1] := zq.Fields[0].AsString;
    sgLignesFact.Rows[sgLignesFact.RowCount-1][2] := zq.Fields[1].AsString;;
    sgLignesFact.Rows[sgLignesFact.RowCount-1][3] := IntToStr(frmListeProduits.Quantite);
    sgLignesFact.Rows[sgLignesFact.RowCount-1][4] := zq.Fields[2].AsString;

    pht := zq.Fields[3].AsFloat * frmListeProduits.Quantite;
    sgLignesFact.Rows[sgLignesFact.RowCount-1][5] := FloatToStr(RoundTo(pht,-2));

    if zq.Fields[2].AsFloat > 0 then
      pttc := pht + ((pht / 100) * zq.Fields[2].AsFloat)
      else
      pttc := pht;
    sgLignesFact.Rows[sgLignesFact.RowCount-1][6] := FloatToStr(RoundTo(pttc,-2));
    sgLignesFact.Rows[sgLignesFact.RowCount-1][7] := '';

    TotHT := TotHT + pht;
    TotTTC := TotTTC + pttc;

    zq.Close;
  end;
  RecalcChamps(Sender);
  edAcompteChange(Sender);
end;

procedure TfrmFactures.miLFPrixClick(Sender: TObject);
var pxStr: string;
  pht,pttc: float;
begin
  pxStr := sgLignesFact.Rows[sgLignesFact.Row][5];
  if InputQuery('Prix personnalisé', 'Entrez le prix désiré', pxStr) then
    if pxStr <> '' then
    begin
      pxStr := BaseFact.ConvToLocalDecSep(pxStr);
      if TryStrToFloat(pxStr,pht) then
      begin
        sgLignesFact.Rows[sgLignesFact.Row][5] := FloatToStr(RoundTo(pht,-2));
        pttc := pht + ((pht / 100) * StrToFloat(sgLignesFact.Rows[sgLignesFact.Row][4]));
        sgLignesFact.Rows[sgLignesFact.Row][6] := FloatToStr(RoundTo(pttc,-2));
        RecalcChamps(Sender);
        edAcompteChange(Sender);
      end else begin
        ShowMessage('Erreur de conversion de chaine vers flottant!');
        miLFPrixClick(Sender);
      end;
    end;
end;

procedure TfrmFactures.miLFRemiseClick(Sender: TObject);
var pxStr: string;
  remise: float;
begin
  pxStr := sgLignesFact.Rows[sgLignesFact.Row][7];
  if InputQuery('Montant HT de la remise', 'Entrez le montant désiré', pxStr) then
    if pxStr <> '' then
    begin
      pxStr := BaseFact.ConvToLocalDecSep(pxStr);

      if TryStrToFloat(pxStr,remise) then
      begin
        sgLignesFact.Rows[sgLignesFact.Row][7] := FloatToStr(RoundTo(remise,-2));
        RecalcChamps(Sender);
        edAcompteChange(Sender);
      end else begin
        ShowMessage('Erreur de conversion de chaine vers flottant!');
        miLFRemiseClick(Sender);
      end;
    end;
end;


procedure TfrmFactures.miLFLibelleClick(Sender: TObject);
var libstr: string;
begin
  libStr := sgLignesFact.Rows[sgLignesFact.Row][2];
  if InputQuery('Libellé personnalisé', 'Entrez le libellé désiré', libStr) then
    if libStr <> '' then
    begin
      sgLignesFact.Rows[sgLignesFact.Row][2] := libStr;
    end;
end;

procedure TfrmFactures.miLFSupprimerClick(Sender: TObject);
var
  i: integer;
  zq: TZQuery;
begin
  sgLignesFact.DeleteRow(sgLignesFact.Row);

  RecalcChamps(Sender);
  edAcompteChange(Sender);
end;

procedure TfrmFactures.miNouvelleClick(Sender: TObject);
begin
  TotHT := 0;
  TotTTC := 0;
  edNom.Clear;
  edPrenom.Clear;
  mmAdresse.Lines.Clear;
  edTotHT.Text := '0,00';
  edRemise.Text := '0,00';
  edTotTTC.Text := '0,00';
  edAcompte.Text := '0,00';
  edSommeDu.Text := '0,00';
  cbClients.ItemIndex := -1;
  edDate.Text := FormatDateTime('dd-mm-YYYY',Now());
  frmPaiements.edfDate.Text := FormatDateTime('dd-mm-YYYY',Now());
  frmPaiements.sgPaiements.RowCount:=1;
  sgLignesFact.RowCount := 1;
  frmListeFactures.FactureID := 0;
  if frmListeProduits.Visible then
    frmListeProduits.btUnEdit.Click;
  if frmListeFactures.Visible then
    frmListeFactures.btUnEdit.Click;
  if frmPaiements.Visible then
    frmPaiements.btFermer.Click;

  frmListeProduits.btAjouter.Enabled := true;
end;

procedure TfrmFactures.miSupprimerClick(Sender: TObject);
var
  zq: TZQuery;
begin
  TotHT := 0;
  TotTTC := 0;
  if frmListeFactures.FactureID > 0 then
  begin
    zq := TZQuery.Create(nil);
    zq.Connection := BaseFact.ZConn;

    zq.SQL.Clear;
    zq.SQL.Add('DELETE FROM paiements WHERE fid = '+QuotedStr(IntToStr(frmListeFactures.FactureID))+';');
    zq.SQL.Add('DELETE FROM lignesfactures WHERE fid = '+QuotedStr(IntToStr(frmListeFactures.FactureID))+';');
    zq.SQL.Add('DELETE FROM factures WHERE id = '+QuotedStr(IntToStr(frmListeFactures.FactureID))+';');
    //Delete paiements
    zq.ExecSQL;
    zq.Free;
  end;

  //frmListeFactures.FactureID := -1;
  frmFactures.miNouvelleClick(Sender);
end;

procedure TfrmFactures.pmLignesFactPopup(Sender: TObject);
var
  ControlCoord, NewCell: TPoint;
begin
  ControlCoord := sgLignesFact.ScreenToControl(pmLignesFact.PopupPoint);
  NewCell := sgLignesFact.MouseToCell(ControlCoord);
  if NewCell.Y > 0 then
    sgLignesFact.Row := NewCell.Y
  else
    sgLignesFact.Row := -1;
end;

procedure TfrmFactures.sgPaiementsSelection(Sender: TObject; aCol, aRow: Integer
  );
begin
  frmPaiements.cbfModePaiement.ItemIndex := frmPaiements.cbfModePaiement.Items.IndexOf(frmPaiements.sgPaiements.Cells[0,aRow]);
  frmPaiements.edfMontant.Text :=frmPaiements.sgPaiements.Cells[1,aRow];
  frmPaiements.edfDate.Text := frmPaiements.sgPaiements.Cells[2,aRow];
  frmPaiements.edfTxId.Text := frmPaiements.sgPaiements.Cells[3,aRow];
end;


procedure TfrmFactures.btValiderClick(Sender: TObject);
var
  sql: string;
  i, FactID: integer;
  dt: TDateTime;
  zq: TZQuery;
begin
  try
    //Check required
    if edNom.Text = '' then
      raise Exception.Create('Le champ "Nom" est obligatoire!');
    if not TryStrToDate(edDate.Text,dt,'DD-MM-YYYY',BaseFact.formatters.DateSeparator) then
      raise Exception.Create('Le champ date est invalide!'+#13#10+'Format accepté : dd-mm-yyyy');
    if sgLignesFact.RowCount <= 1  then
      raise Exception.Create('La facture ne comporte aucun produit/prestation!');

    //Fill database
    try
      zq := TZQuery.Create(nil);
      zq.Connection := BaseFact.ZConn;

      if frmListeFactures.FactureID <= 0 then
      begin
        sql := 'INSERT INTO factures (etat,dt,nom,prenom,adresse) VALUES('
            +QuotedStr(IntToStr(cbEtat.ItemIndex))+','
            +QuotedStr(IntToStr(DateTimeToUnix(dt)))+','
            +QuotedStr(edNom.Text)+','
            +QuotedStr(edPrenom.Text)+','
            +QuotedStr(mmAdresse.Lines.Text)+');';
        zq.SQL.Text := sql;
        zq.ExecSQL;

        //Get Last insert ID from Factures
        sql := 'SELECT MAX(id) FROM factures;';
        zq.SQL.Text := sql;
        zq.Open;
        FactID := zq.Fields[0].AsInteger;
        zq.Close;
      end else
      begin
        sql := 'UPDATE factures SET '
            +'etat='+IntToStr(cbEtat.ItemIndex)+','
            +'dt='+IntToStr(DateTimeToUnix(dt))+','
            +'nom='+QuotedStr(edNom.Text)+','
            +'prenom='+QuotedStr(edPrenom.Text)+','
            +'adresse='+QuotedStr(mmAdresse.Lines.Text)
            +' WHERE id = '+IntToStr(frmListeFactures.FactureID)+';';

        zq.SQL.Text := sql;
        zq.ExecSQL;

        FactID := frmListeFactures.FactureID;

        zq.SQL.Clear;
        zq.SQL.Add('DELETE FROM lignesfactures WHERE fid = '+QuotedStr(IntToStr(FactID))+';');
        zq.ExecSQL;
      end;


    //Insert products lines in LignesFactures
      for i := 1 to sgLignesFact.RowCount - 1 do
      begin
        sql := 'INSERT INTO lignesfactures (fid,`type`,nom,qt,tva,prix,remise) VALUES('
              +IntToStr(FactID)+','
              +QuotedStr(sgLignesFact.Rows[i][1])+','
              +QuotedStr(sgLignesFact.Rows[i][2])+','
              +QuotedStr(sgLignesFact.Rows[i][3])+','
              +QuotedStr(StringReplace(sgLignesFact.Rows[i][4],',','.',[]))+','
              +QuotedStr(StringReplace(sgLignesFact.Rows[i][5],',','.',[]))+','
              +QuotedStr(StringReplace(sgLignesFact.Rows[i][7],',','.',[]))+');';
        zq.SQL.Text := sql;
        zq.ExecSQL;
      end;

      frmListeFactures.FactureID := FactID;
      EditerFactures(frmListeFactures.FactureID);

    finally
      zq.Free;
    end;

    except on e: Exception do
      if e.Message <> '' then
        ShowMessage(e.Message);
  end;
end;

procedure TfrmFactures.btGererClick(Sender: TObject);
begin
  frmPaiements.Visible := true;
  frmListeFactures.Visible := false;
  frmListeProduits.Visible := false;
  GroupBox2.Visible := false;
  if cbEtat.ItemIndex <> LongInt(feDevis) then
  begin
    if frmPaiements.sgPaiements.RowCount > 1 then
    begin
      frmPaiements.sgPaiements.Row:=1;
      sgPaiementsSelection(Sender,0,1);
    end else
    begin
      frmPaiements.edfMontant.Clear;
      frmPaiements.edfTxId.Clear;
      frmPaiements.edfDate.Text := FormatDateTime('dd-mm-YYYY',Now(),BaseFact.formatters);
    end;
    while frmPaiements.Visible = true do
      Application.ProcessMessages;
  end else ShowMessage('Vous devez d''abord basculer le devis en facture!');

  GroupBox2.Visible:=true;
end;

procedure TfrmFactures.btEditerClick(Sender: TObject);
begin

end;

procedure TfrmFactures.btFermerClick(Sender: TObject);
begin
  frmPaiements.Visible := false;
end;

procedure TfrmFactures.btfSupprimerClick(Sender: TObject);
var pid: integer;
  sqlQuery: string;
  zq: TZQuery;
begin
   if (frmPaiements.sgPaiements.Rows[frmPaiements.sgPaiements.Row][4] <> '')
     and TryStrToInt(frmPaiements.sgPaiements.Rows[frmPaiements.sgPaiements.Row][4],pid) then
   begin
     if pid > 0 then
     begin
       sqlQuery := 'DELETE FROM paiements WHERE id = '+IntToStr(pid)+';';

       zq := TZQuery.Create(nil);
       zq.Connection := BaseFact.ZConn;
       zq.SQL.Add(sqlQuery);
       zq.ExecSQL;
       zq.Close;
       zq.Free;

       EditerFactures(frmListeFactures.FactureID);
     end;
   end;
end;

procedure TfrmFactures.btfValiderClick(Sender: TObject);
var sqlQuery,pxStr:string;
  montant: Extended;
  PaiementID: int64;
  dt: TDateTime;
  zq: TZQuery;
begin
  try
    //Check required
    pxStr := BaseFact.ConvToLocalDecSep(frmPaiements.edfMontant.Text);
    if (not TryStrToFloat(pxStr,montant)) or (montant <= 0) then
      raise Exception.Create('Le champ "Montant" est obligatoire!'+#13#10+'Doit être de la forme 1,23 ou 1.23 et supérieur à 0!');
    if not TryStrToDate(frmPaiements.edfDate.Text,dt,'DD-MM-YYYY',BaseFact.formatters.DateSeparator) then
      raise Exception.Create('Le champ date est invalide!'+#13#10+'Format accepté : dd-mm-yyyy');
    if frmPaiements.cbfModePaiement.ItemIndex < 0 then
      raise Exception.Create('Le champ "Mode de paiement" est invalide!');
    if frmListeFactures.FactureID = 0 then
      raise Exception.Create('Veuillez valider la facture avant le paiement, merci.');
    sqlQuery := Format('INSERT INTO paiements (fid,montant,modepment,dt,txid) VALUES(%.04D,"%S","%S",%D,"%S");',
                   [frmListeFactures.FactureID,StringReplace(FloatToStrF(montant,ffFixed,4,2),',','.',[]),frmPaiements.cbfModePaiement.Text,DateTimeToUnix(dt),frmPaiements.edfTxId.Text]);

    zq := TZQuery.Create(nil);
    zq.Connection := BaseFact.ZConn;
    zq.SQL.Text := sqlQuery;
    zq.ExecSQL();

    if (StrToFloat(edTotTTC.Text) - StrToFloat(edRemise.Text)) - (montant + StrToFloat(edAcompte.Text)) = 0 then
    begin
      sqlQuery := 'UPDATE factures SET etat = '+IntToStr(Integer(fePayee))+' WHERE id = '+IntToStr(frmListeFactures.FactureID)+';';
      zq.SQL.Text := sqlQuery;
      zq.ExecSQL();
    end;
    zq.Close;
    zq.Free;

    EditerFactures(frmListeFactures.FactureID);

    except on e:Exception do
      if e.Message <> '' then
        ShowMessage(e.Message);
  end;
end;







{******************************************************************************}
{*******************************  Printers  ***********************************}
{******************************************************************************}
procedure TfrmFactures.ImprimeFacture();
var
  rg: TRegExpr;
  RatioL, RatioH: Extended;
  XPos, YPos, hDoc, hLn, wLn: LongInt;
  s: string;
  i, cntLn, nbLn, numPg, nbPg, W, H, mW: LongInt;
  dt: TDateTime;
  sl: TStringList;
  Prn: TPrinter;

  procedure TraceEntete();
  var i: integer;
  begin
    TryStrToDate(edDate.Text,dt,'DD-MM-YYYY','-');
    // setting font
    Prn.Canvas.Font.Name := 'Arial';
    Prn.Canvas.Font.Size := Round(RatioH*12);

    //Print Entreprise Info
    Prn.Canvas.TextOut(Round(RatioL*400), Round(RatioH*500), lbNomSociete.Caption);

    sl := TStringList.Create;
    sl.Text := lbInfosSociete.Caption;
    YPos := Round(RatioH*750);

    Prn.Canvas.Font.Name := 'Arial';
    Prn.Canvas.Font.Size := Round(RatioH*10);
    for i := 0 to sl.Count - 1 do
    begin
      if sl[i] <> '' then
      begin
        Prn.Canvas.TextOut(Round(RatioL*400), Round(RatioH*YPos), sl[i]);
        YPos := YPos + Round(RatioH*120);
      end else
        YPos := YPos + Round(RatioH*20);
    end;

    Prn.Canvas.Font.Name := 'Arial';
    Prn.Canvas.Font.Size := Round(RatioH*14);
    S := Format('%.4d',[frmListeFactures.FactureID])+'D'+FormatDateTime('DDMMYY',dt);

     if(cbEtat.ItemIndex = LongInt(feDevis)) then
      Prn.Canvas.TextOut(Round(RatioL*(mW + 300)), Round(RatioH*520), 'Réf. devis : D'+S)
    else if(cbEtat.ItemIndex <> LongInt(feDevis)) then
      Prn.Canvas.TextOut(Round(RatioL*(mW+300)), Round(RatioH*520), 'Réf. facture : F' + S);

    //Print Client info
    Prn.Canvas.Font.Name := 'Arial';
    Prn.Canvas.Font.Size := Round(RatioH*12);
    Prn.Canvas.TextOut(Round(RatioL*(mW+600)), Round(RatioH*1400), edNom.Text + ' ' + edPrenom.Text);

    Prn.Canvas.Font.Name := 'Arial';
    Prn.Canvas.Font.Size := Round(RatioH*10);
    YPos := Round(RatioH*1550);
    for i := 0 to mmAdresse.Lines.Count - 1 do
    begin
      Prn.Canvas.TextOut(Round(RatioL*(mW+600)), Round(RatioH*YPos), mmAdresse.Lines[i]);
      YPos := YPos + Round(RatioH*100);
    end;

    Prn.Canvas.Font.Name := 'Arial';
    Prn.Canvas.Font.Size := Round(RatioH*9);

    S := 'Le ' + edDate.Text + ' à Strasbourg';
//    w := w + Round(TextWidth(S));
    Prn.Canvas.TextOut(Round(RatioL*400), Round(RatioH*1650), S);

    YPos := Round(RatioH*2000);
  end;

  procedure TracePied2Page(pg: integer);
  begin
    Prn.Canvas.Line(Round(RatioL*250), Round(RatioH*6700), Round(RatioL*4760), Round(RatioH*6700));
    Prn.Canvas.Font.Name:='Times-Roman';
    Prn.Canvas.Font.Size := Round(RatioH*9);
    S := 'TVA non applicable, art. 293B du CGI.';
    Prn.Canvas.TextOut(mW - (mW div 3),Round(RatioH*6720),S);
    S := 'Page '+IntToStr(pg)+' de '+IntToStr(nbPg);
    Prn.Canvas.TextOut(W - (Prn.Canvas.TextWidth(S) + 200),Round(RatioH*6720),S);
  end;

  procedure TraceLigneFacture(idxLn: longint);
  var wl,hl,lh,tmp: integer;
      textarea: TRect;
      textstyle: TTextStyle;
  begin
    FillChar(textstyle, SizeOf(TTextStyle), 0);
    wl := Prn.Canvas.TextWidth(sgLignesFact.Rows[idxLn][2]);
    hl := Prn.Canvas.TextHeight(sgLignesFact.Rows[idxLn][2]);
    lh := ceil(wl/(2150*RatioL))*hl;
    if wl/(2150*RatioL) > 1 then
    begin
      textarea := Rect(Round(250*RatioL), YPos, Round(2350 * RatioL), YPos + Round((lh + 100) * RatioH));
      textstyle := Prn.Canvas.TextStyle;
      textstyle.Opaque := True;
      textstyle.Wordbreak := True;
      textstyle.SingleLine := False;
      Prn.Canvas.TextRect(textarea, Round(RatioL*250), YPos + Round(RatioH*60) , sgLignesFact.Rows[idxLn][2], textstyle);
    end else
      Prn.Canvas.TextOut(Round(RatioL*250), YPos + Round(RatioH*60), sgLignesFact.Rows[idxLn][2]);

    s := sgLignesFact.Rows[idxLn][3];
    Prn.Canvas.TextOut(Round(RatioL*2450), YPos + Round(RatioH*60), s);

    s := sgLignesFact.Rows[idxLn][4];
    Prn.Canvas.TextOut(Round(RatioL*3050), YPos + Round(RatioH*60), s);
    Prn.Canvas.TextOut(Round(RatioL*3450), YPos + Round(RatioH*60), Format('%.2F',[StrToFloat(sgLignesFact.Rows[idxLn][5])]));
    Prn.Canvas.TextOut(Round(RatioL*4050), YPos + Round(RatioH*60), Format('%.2F',[StrToFloat(sgLignesFact.Rows[idxLn][6])]));

    Prn.Canvas.MoveTo(Round(RatioL*200), YPos + Round((lh+100) * RatioH) );
    Prn.Canvas.LineTo(Round(RatioL*4600), YPos + Round((lh+100) * RatioH) ); // + Round(RatioH*1400)

    XPos := Round(RatioL*200);
    Prn.Canvas.Line(XPos, YPos , XPos, YPos + Round((lh+100) * RatioH));

    XPos := Round(RatioL*2400);
    Prn.Canvas.Line(XPos, YPos , XPos, YPos + Round((lh+100) * RatioH));

    XPos := Round(RatioL*3000);
    Prn.Canvas.Line(XPos, YPos , XPos, YPos + Round((lh+100) * RatioH));

    XPos := Round(RatioL*3400);
    Prn.Canvas.Line(XPos, YPos , XPos, YPos + Round((lh+100) * RatioH));

    XPos := Round(RatioL*4000);
    Prn.Canvas.Line(XPos, YPos , XPos, YPos + Round((lh+100) * RatioH));

    XPos := Round(RatioL*4600);
    Prn.Canvas.Line(XPos, YPos , XPos, YPos + Round((lh+100) * RatioH));

    YPos := YPos + Round((lh+100) * RatioH);
  end;

  function TraceLignesEntete (): integer;
  begin
    // writing the header of the text.
    Prn.Canvas.Font.Name := 'Times-Roman';
    Prn.Canvas.Font.Size := Round(RatioH*10);

    Prn.Canvas.Frame(Round(RatioL*200),YPos,Round(RatioL*4600),YPos + Round(RatioH*200));

    XPos := Round(RatioL*200);
    Prn.Canvas.TextOut(XPos + Round(50 * RatioL), YPos + Round(RatioH*60), 'Produit/Prestation');

    XPos := Round(RatioL*2400);
    Prn.Canvas.Line(XPos, YPos , XPos, YPos + Round(RatioH*200));
    Prn.Canvas.TextOut(XPos + Round(50 * RatioL),YPos + Round(RatioH*60), 'Quantité');

    XPos := Round(RatioL*3000);
    Prn.Canvas.Line(XPos, YPos , XPos, YPos + Round(RatioH*200));
    Prn.Canvas.TextOut(XPos + Round(50 * RatioL), YPos + Round(RatioH*60), 'TVA');

    XPos := Round(RatioL*3400);
    Prn.Canvas.Line(XPos, YPos , XPos, YPos + Round(RatioH*200));
    Prn.Canvas.TextOut(XPos + Round(50 * RatioL),YPos + Round(RatioH*60), 'Prix HT');

    XPos := Round(RatioL*4000);
    Prn.Canvas.Line(XPos, YPos , XPos, YPos + Round(RatioH*200));
    Prn.Canvas.TextOut(XPos + Round(50 * RatioL), YPos + Round(RatioH*60), 'Prix TTC');

    result := YPos + Round(RatioH*200);
  end;

  function TraceLignesFacture(): integer;
  var i: integer;
    bottomY,wl,lh,hl: longint;
  begin
    result := 0;

    bottomY :=Prn.PageHeight-Round(400*RatioH);
    // writing the outline
    YPos := YPos + Round(RatioH*100);

    YPos := TraceLignesEntete();

    lh := 0;
    for i := 1 to nbLn - 1 do
    begin
      wl := Prn.Canvas.TextWidth(sgLignesFact.Rows[i][2]);
      hl := Prn.Canvas.TextHeight(sgLignesFact.Rows[i][2]);
      lh := ceil(wl/Round(2150*RatioL)*hl);
      if YPos + lh < bottomY then
      begin
        TraceLigneFacture(i);
      end else
      begin
        TracePied2Page(numPg);
        Inc(numPg);
        Prn.NewPage;
        YPos := Round(200*RatioH);
        YPos := TraceLignesEntete();
        TraceLigneFacture(i);
      end;
    end;
  end;

  procedure TraceTotaux();
  var startY: integer;
  begin
    startY := YPos;
    Prn.Canvas.Pen.Width := Round(RatioH*2);

    YPos := YPos + Round(RatioH*100);
    Prn.Canvas.Rectangle(Round(RatioL*(mW+400)), YPos, Round(RatioL*(mW+1200)), YPos + Round(RatioH*200));
    Prn.Canvas.Rectangle(Round(RatioL*(mW+1200)), YPos, Round(RatioL*(mW+2000)), YPos + Round(RatioH*200));
    YPos := YPos + Round(RatioH*200);
    Prn.Canvas.Rectangle(Round(RatioL*(mW+400)), YPos, Round(RatioL*(mW+1200)), YPos + Round(RatioH*200));
    Prn.Canvas.Rectangle(Round(RatioL*(mW+1200)), YPos, Round(RatioL*(mW+2000)), YPos + Round(RatioH*200));
    YPos := YPos + Round(RatioH*200);
    Prn.Canvas.Rectangle(Round(RatioL*(mW+400)), YPos, Round(RatioL*(mW+1200)), YPos + Round(RatioH*200));
    Prn.Canvas.Rectangle(Round(RatioL*(mW+1200)), YPos, Round(RatioL*(mW+2000)), YPos + Round(RatioH*200));
    YPos := YPos + Round(RatioH*200);
    Prn.Canvas.Rectangle(Round(RatioL*(mW+400)), YPos, Round(RatioL*(mW+1200)), YPos + Round(RatioH*200));
    Prn.Canvas.Rectangle(Round(RatioL*(mW+1200)), YPos, Round(RatioL*(mW+2000)), YPos + Round(RatioH*200));
    YPos := YPos + Round(RatioH*200);
    Prn.Canvas.Rectangle(Round(RatioL*(mW+400)), YPos, Round(RatioL*(mW+1200)), YPos + Round(RatioH*200));
    Prn.Canvas.Rectangle(Round(RatioL*(mW+1200)), YPos, Round(RatioL*(mW+2000)), YPos + Round(RatioH*200));

    Prn.Canvas.Pen.Width := 1;
    YPos := startY;

    Prn.Canvas.TextOut(Round(RatioL*(mW+450)),YPos + Round(RatioH*150),'Total HT');
    Prn.Canvas.TextOut(Round(RatioL*(mW+1250)),YPos + Round(RatioH*150),Format('%.2F',[StrToFloat(edTotHT.Text)]));
    YPos := YPos + Round(RatioH*200);
    Prn.Canvas.TextOut(Round(RatioL*(mW+450)),YPos + Round(RatioH*150),'Total TTC');
    Prn.Canvas.TextOut(Round(RatioL*(mW+1250)),YPos + Round(RatioH*150),Format('%.2F',[StrToFloat(edTotTTC.Text)]));
    YPos := YPos + Round(RatioH*200);
    Prn.Canvas.TextOut(Round(RatioL*(mW+450)),YPos + Round(RatioH*150),'Remise');
    Prn.Canvas.TextOut(Round(RatioL*(mW+1250)),YPos + Round(RatioH*150),Format('%.2F',[StrToFloat(edRemise.Text)]));
    YPos := YPos + Round(RatioH*200);
    Prn.Canvas.TextOut(Round(RatioL*(mW+450)),YPos + Round(RatioH*150),'Acompte');
    Prn.Canvas.TextOut(Round(RatioL*(mW+1250)),YPos + Round(RatioH*150),Format('%.2F',[StrToFloat(edAcompte.Text)]));
    YPos := YPos + Round(RatioH*200);
    Prn.Canvas.TextOut(Round(RatioL*(mW+450)),YPos + Round(RatioH*150),'Net à payer');
    Prn.Canvas.TextOut(Round(RatioL*(mW+1250)),YPos + Round(RatioH*150),Format('%.2F',[StrToFloat(edSommeDu.Text)]));
  end;

begin
  Prn := Printer;
  //Resolution de base L 4961 x H 7016 sur mon imprimante donc dessin en fonction
  W := Prn.PaperSize.Width;
  mW := W div 2;
  H := Prn.PaperSize.Height;
  RatioL := W / 4960;
  RatioH := H / 7015;

  wLn := 0;
  hLn := 0;
  hDoc := 0;

  nbLn := sgLignesFact.RowCount;

  numPg := 1;

  {* Traçage *}
  TraceEntete();

  Prn.Canvas.Font.Name := 'Times-Roman';
  Prn.Canvas.Font.Size := Round(RatioH*10);

  {* TODO: Recalculer le nombre de page en caculant textheight + marge de chaque ligne + header +totaux + footer *}
  for i := 1 to nbLn - 1 do
  begin
    wLn := Prn.Canvas.TextWidth(sgLignesFact.Rows[i][2]);
    hLn := Prn.Canvas.TextHeight(sgLignesFact.Rows[i][2]);
    hDoc := hDoc + (ceil(wLn/Round(2150*RatioL))*hLn) + 100;
  end;
  nbPg := ceil((2200+hDoc+1800)/((H / RatioH)-400));

  YPos := Round(2250*RatioH);

  TraceLignesFacture();

  if YPos + 1800 > (H / RatioH) - 200 then
  begin
    TracePied2Page(numPg);
    Prn.NewPage;
    YPos := 400;
    Inc(numPg);
  end;

  TraceTotaux();

  if(cbEtat.ItemIndex = LongInt(feDevis)) then
  begin
    YPos := (YPos - Round(RatioH*1000)) + Round(RatioH*220);
    Prn.Canvas.Font.Size := Round(RatioH*9);
    //Phrase et consigne devis à gauche des totaux
    i := 1;
    s := BaseFact.GetCfgVal('devis');
    rg := TRegExpr.Create;
    rg.InputString:=s;
    rg.Expression:={$IFDEF LINUX}'([^\n]+)+'{$ELSE}'([^\r\n]+)+'{$ENDIF};
    rg.ModifierStr:='igm';
    if rg.Exec then
    repeat
      Prn.Canvas.TextOut(Round(RatioL*200), YPos + Round(RatioH*(i*120)) , rg.Match[1]);
      Inc(i);
    until not rg.ExecNext;
    rg.Free;

    //Signature et date à peu près sous le total
    YPos := YPos + Round(RatioH*1100);
    Prn.Canvas.Font.Size := Round(RatioH*10);
    i := 1;
    s := BaseFact.GetCfgVal('accord');
    rg := TRegExpr.Create;
    rg.InputString:=s;
    rg.Expression:={$IFDEF LINUX}'([^\n]+)+'{$ELSE}'([^\r\n]+)+'{$ENDIF};
    rg.ModifierStr:='igm';
    if rg.Exec then
    repeat
      Prn.Canvas.TextOut(mW + Round(RatioL*500), YPos + Round(RatioH*(i*120)), rg.Match[1] );
      Inc(i);
    until not rg.ExecNext;
    rg.Free;
  end else if(frmPaiements.sgPaiements.RowCount > 1) then
  begin
    YPos := (YPos - Round(RatioH*800)) + Round(RatioH*220);
    Prn.Canvas.TextOut(Round(RatioL*200), YPos + 12, 'Paiement(s)');
    YPos := YPos + Round(RatioH*150);
    Prn.Canvas.Font.Name := 'Times-Roman';
    Prn.Canvas.Font.Size := Round(RatioH*8);
    Prn.Canvas.TextOut(Round(RatioL*400), YPos ,'Type');
    Prn.Canvas.TextOut(Round(RatioL*1000), YPos,'Référence');
    Prn.Canvas.TextOut(Round(RatioL*1800), YPos,'Date');
    Prn.Canvas.TextOut(Round(RatioL*2400), YPos,'Montant');
    YPos := YPos + Round(RatioH*120);
    for i := 1 to frmPaiements.sgPaiements.RowCount - 1 do
    begin
      if H - YPos < 200 then
      begin
        TracePied2Page(numPg);
        Inc(numPg);
        Prn.NewPage;
        YPos := Round(400*RatioH);
      end;
      Prn.Canvas.TextOut(Round(RatioL*400), Round(RatioH*YPos), frmPaiements.sgPaiements.Cells[0,i]);
      Prn.Canvas.TextOut(Round(RatioL*1000), Round(RatioH*YPos), frmPaiements.sgPaiements.Cells[3,i]);
      Prn.Canvas.TextOut(Round(RatioL*1800), Round(RatioH*YPos), frmPaiements.sgPaiements.Cells[2,i]);
      Prn.Canvas.TextOut(Round(RatioL*2400), Round(RatioH*YPos), frmPaiements.sgPaiements.Cells[1,i]+' €');
      YPos := YPos + Round(RatioH*100);
    end;
  end;

  TracePied2Page(nbPg);
end;



{******************************************************************************}
{**********************************  PDF  *************************************}
{******************************************************************************}

procedure TfrmFactures.TextOut(X, Y: Single; S: string);
begin
  with PdfDoc.Canvas do
  begin
    BeginText;
    MoveTextPoint(X, Y);
    ShowText(S);
    EndText;
  end;
end;

procedure TfrmFactures.DrawLine(X1, Y1, X2, Y2, W: Single);
begin
  with PdfDoc.Canvas do
  begin
    MoveTo(X1, Y1);
    LineTo(X2, Y2);
    Stroke;
  end;
end;


procedure TfrmFactures.GenerePDF(Filename: String);
var
  s: string;
  i, nbPg, nbTotLn, nbLnTrc, h, w, mw, idxLn: integer;
  XPos, YPos, tmp: Single;
  dt: TDateTime;
  sl: TStringList;
  fout: TFileStream;
  pr: TPdfRect;

  procedure TraceEntetePDF();
  var i: integer;
  begin
    with PdfDoc.Canvas do
    begin
      TryStrToDate(edDate.Text,dt,'DD-MM-YYYY','-');

      if(cbEtat.ItemIndex = LongInt(feDevis)) then
      begin
        SetFont('Arial', 16);
        S := Format('%.4d',[frmListeFactures.FactureID])+'D'+FormatDateTime('DDMMYY',dt);
//        S := Format('%s%.02d%.04d',[Copy(IntToStr(YearOf(dt)),3,2),MonthOf(dt),frmListeFactures.FactureID]);
        TextOut(mw, 788, 'Réf. devis : D' + S);
      end else
      begin
        SetFont('Arial', 18);
        TextOut(mw, 788, 'FACTURE');
      end;
      // setting font
      SetFont('Arial', 14);

      //Print Entreprise Info
      TextOut(60, 770, StringReplace(lbNomSociete.Caption,'&&','&',[rfReplaceAll]));

      sl := TStringList.Create;
      sl.Text := StringReplace(lbInfosSociete.Caption,'&&','&',[rfReplaceAll]);
      YPos := 736;
      SetFont('Arial', 10);
      for i := 0 to sl.Count - 1 do
      begin
        TextOut(60, YPos, sl[i]);
        if sl[i] = '' then
          YPos := YPos - 8
          else YPos := YPos - 18;
      end;

      //Print Client info
      SetFont('Arial', 14);
      TextOut(mw+70, 700, edNom.Text + ' ' + edPrenom.Text);

      SetFont('Arial', 12);
      YPos := 680;
      for i := 0 to mmAdresse.Lines.Count - 1 do
      begin
        TextOut(mw+70, YPos, mmAdresse.Lines[i]);
        YPos := YPos - 18;
      end;

      SetFont('Arial', 10);

      if(cbEtat.ItemIndex <> LongInt(feDevis)) then
      begin
        S := Format('%.4d',[frmListeFactures.FactureID])+'D'+FormatDateTime('DDMMYY',dt);
//        S := Format('%s%.02d%.04d',[Copy(IntToStr(YearOf(dt)),3,2),MonthOf(dt),frmListeFactures.FactureID]);
        TextOut(60, 620, 'Réf. facture : F'+S);
      end;

      S := 'Le ' + edDate.Text + ' à Strasbourg';
      TextOut(mw + 40, 600, S);
      Stroke;
    end;
  end;

  procedure TracePied2PagePDF();
  begin
    with PdfDoc.Canvas do
    begin
      DrawLine(70, 32, 530, 32, 1);
      SetFont('Times-Roman', 8);

      S := 'TVA non applicable, art. 293B du CGI.';
      TextOut(mw - (MeasureText(S,w*2)+35),20,S);
      Stroke;
    end;
  end;

  function EcrireLigneFacture():cardinal;
  var
    i, fi, nbLn, nbCh: integer;
    s, tmp: string;
    y: Single;

  begin
    Result := 0;
    // printing the detail lines
    with PdfDoc.Canvas do
    begin
      y := YPos;
      idxLn := 1;
      while idxLn < sgLignesFact.RowCount do
      begin
        XPos := 70;

        s := sgLignesFact.Cells[2,idxLn];
        i := MeasureText(s, 240);
        nbLn := 0;
        repeat
          tmp := Copy(s,1,i);
          nbCh := Length(s);
          fi := i;
          if (tmp[Length(tmp)] <> ' ') and (nbCh > Length(tmp)) then
          begin
            for fi := Length(tmp) downto 1 do
              if tmp[fi] = ' ' then
                break;
          end;
          if fi = 1 then fi := i;
          TextOut(XPos + 5, (y - 15) - (nbLn * 15),Copy(s,1,fi));
          s := Copy(s, fi+1, Length(s) - fi);
          Inc(nbLn);
          i := MeasureText(s, 240);
        until (s = '');

        Rectangle(XPos, y, 250, -((nbLn * 15) + 5));

        XPos := 320;
        Rectangle(XPos, y, 50, -((nbLn * 15) + 5));
        TextOut(XPos + 5, y - 15, sgLignesFact.Cells[3,idxLn]);

        XPos := 370;
        Rectangle(XPos, y, 50, -((nbLn * 15) + 5));
        TextOut(XPos + 5, y - 15, sgLignesFact.Cells[4,idxLn]);

        XPos := 420;
        Rectangle(XPos, y, 50, -((nbLn * 15) + 5));
        TextOut(XPos + 5, y - 15, Format('%.2F',[StrToFloat(sgLignesFact.Cells[5,idxLn])]));

        XPos := 470;
        Rectangle(XPos, y, 50, -((nbLn * 15) + 5));
        TextOut(XPos + 5, y - 15, Format('%.2F',[StrToFloat(sgLignesFact.Cells[6,idxLn])]));


        if (y < 100) then
        begin
          TracePied2PagePDF();
          y := 800;
          Inc(IdxLn);
          PdfDoc.AddPage();
          SetLineWidth(1.5);
          SetFont('Times-Roman', 10.5);
        end else
        begin
          y := (y - 5) - (nbLn * 15);
          Inc(IdxLn);
        end;
        Inc(Result, nbLn);
      end;
      YPos := y;
    end;
  end;

  function TraceLignesFacturesPDF():cardinal;
  var StartY: Single;
  begin
    result := 0;
    with PdfDoc.Canvas do
    begin
      startY := YPos;

      // writing the header of the text.
      SetLineWidth(1.5);
      SetFont('Times-Roman', 10.5);

      XPos := 70;
      Rectangle(XPos, YPos, 250, -20);
      TextOut(XPos + 5, YPos - 15, 'Produit/Prestation');

      XPos := 320;
      Rectangle(XPos, YPos, 50, -20);
      TextOut(XPos + 5, YPos - 15, 'Quantité');

      XPos := 370;
      Rectangle(XPos, YPos, 50, -20);
      TextOut(XPos + 5, YPos - 15, 'TVA');

      XPos := 420;
      Rectangle(XPos, YPos, 50, -20);
      TextOut(XPos + 5, YPos - 15, 'Prix HT');

      XPos := 470;
      Rectangle(XPos, YPos, 50, -20);
      TextOut(XPos + 5, YPos - 15, 'Prix TTC');

      Stroke;

      YPos := YPos - 20;

      // writing Bill lines values
      Result := EcrireLigneFacture();

      Stroke;
    end;
  end;

  procedure TraceTotauxPDF();
  begin
    with PdfDoc.Canvas do
    begin
    SetLineWidth(1);

    if YPos < 120 then
    begin
      TracePied2PagePDF();
      PdfDoc.AddPage();
      YPos := 800;
    end;

    YPos := YPos - 20;

    Rectangle(380, YPos, 70, 20);
    Rectangle(450, YPos, 70, 20);
    TextOut(385,YPos + 5,'Total HT');
    TextOut(455,YPos + 5,Format('%.2F',[StrToFloat(edTotHT.Text)]));

    YPos := YPos - 20;

    Rectangle(380, YPos, 70, 20);
    Rectangle(450, YPos, 70, 20);
    TextOut(385, YPos + 5, 'Total TTC');
    TextOut(455, YPos + 5, Format('%.2F',[StrToFloat(edTotTTC.Text)]));

    YPos := YPos - 20;

    Rectangle(380, YPos, 70, 20);
    Rectangle(450, YPos, 70, 20);
    TextOut(385, YPos + 5, 'Remise');
    TextOut(455, YPos + 5, Format('%.2F',[StrToFloat(edRemise.Text)]));

    YPos := YPos - 20;

    Rectangle(380, YPos, 70, 20);
    Rectangle(450, YPos, 70, 20);
    TextOut(385, YPos + 5, 'Acompte');
    TextOut(455, YPos + 5, Format('%.2F',[StrToFloat(edAcompte.Text)]));

    YPos := YPos - 20;

    Rectangle(380, YPos, 70, 20);
    Rectangle(450, YPos, 70, 20);
    TextOut(385, YPos + 5, 'Net à Payer');
    TextOut(455, YPos + 5, Format('%.2F',[StrToFloat(edSommeDu.Text)]));

    Stroke;
    end;
  end;

  procedure PrintMl(ARect:TPdfRect;S:String; border: boolean) ;
var CountLine:Single;
begin
  with PDFDoc.Canvas do     begin
    SetFont('Arial', 10);
    SetLeading(10);
    CountLine:=MultilineTextRectCountLine (ARect,  S, True);
    If Arect.Top -(CountLine*Attribute.FontSize)<0 Then Begin
       WriteLN('Error, row  outside the line margin');
    end else begin
       ARect.Bottom:=Arect.Top -(CountLine*Attribute.FontSize);
       MultilineTextRect(ARect, S, True);
       if border = true then
         with ARect do begin
           MoveTo(Left, Top);
           LineTo(Left, Bottom);
           LineTo(Right, Bottom);
           LineTo(Right, Top);
           LineTo(Left, Top);
        end;
    end;
  end;
end;

begin
  if Assigned(PdfDoc) then
    FreeandNil(PdfDoc);

  PdfDoc := TPdfDoc.Create;

  PdfDoc.NewDoc;

  PdfDoc.AddPage;

  nbPg := 1;

  with PdfDoc.Canvas do
  begin
    h := PageHeight;
    w := PageWidth;
    mw := w div 2;
    nbTotLn := sgLignesFact.RowCount;

    //Tracage entete
    TraceEntetePDF();

    XPos := 70;
    //Hauteur de départ de la 1ere ligne de facture de la 1ere page
    YPos := 580;
    idxLn := 1;
    nbLnTrc := TraceLignesFacturesPDF();

    tmp := YPos;

    YPos := YPos - 20;
    TraceTotauxPDF();
    YPos := tmp;

    if(cbEtat.ItemIndex = LongInt(feDevis)) then
    begin
      if YPos - 183 < 20 then
      begin
        tmp := h;
      end;
      YPos := tmp - 30;
      //Phrase et consigne devis à gauche des totaux
      pr.Left := 70;
      pr.Top := YPos - 3;
      pr.Right := mw + 60;
      pr.Bottom := YPos - 180;
      PrintMl(pr,BaseFact.GetCfgVal('devis'),false);
      //MultilineTextRect(pr, BaseFact.GetCfgVal('devis'),true);

      pr.Left := mW + 30;
      pr.Top := YPos - 143;
      pr.Right := mw + 250;
      pr.Bottom := YPos - 153;
      PrintMl(pr,BaseFact.GetCfgVal('accord'),false);
      //MultilineTextRect(pr,  BaseFact.GetCfgVal('accord'),false);
    end else if(frmPaiements.sgPaiements.RowCount > 1) then
    begin
      if YPos < 120 then
      begin
        tmp := h;
      end;
      YPos := tmp - 50;
      TextOut(160, YPos + 12, 'Paiement(s)');
      SetFont('Times-Roman', 8);
      TextOut(70, YPos ,'Type');
      TextOut(120, YPos,'Référence');
      TextOut(200, YPos,'Date');
      TextOut(260, YPos,'Montant');
      YPos := YPos - 10;
      for i := 1 to frmPaiements.sgPaiements.RowCount - 1 do
      begin
        if YPos - 173 < 10 then
        begin
          TracePied2PagePDF();
          PdfDoc.AddPage();
          YPos := h;
        end;
        TextOut(70, YPos, frmPaiements.sgPaiements.Cells[0,i]);
        TextOut(120, YPos, frmPaiements.sgPaiements.Cells[3,i]);
        TextOut(200, YPos, frmPaiements.sgPaiements.Cells[2,i]);
        TextOut(260, YPos, frmPaiements.sgPaiements.Cells[1,i]+' €');
        YPos := YPos - 10;
      end;
    end;

    TracePied2PagePDF();
  end;

  if Filename <> '' then
  begin
    try
      fout := TFileStream.Create(Filename, fmCreate);
      PdfDoc.SaveToStream(fout);
      fout.Free;

      except on e:Exception do
        ShowMessage('Le fichier "'+Filename+'" n''a pu être créé!');
    end;
  end;
  FreeAndNil(PdfDoc);
end;
end.
