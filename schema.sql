CREATE TABLE config (id integer primary key autoincrement, vt integer, nom varchar(32) not null, valeur varchar(10000));
CREATE TABLE clients (id integer primary key autoincrement, nom varchar(50) not null, prenom varchar(50), adresse varchar(250), phone varchar(20), phone2 varchar(20), email varchar(128));
CREATE TABLE tvas (id integer primary key autoincrement, taux float not null);
CREATE TABLE produits (id integer primary key autoincrement, nom varchar(250) not null, tvaid integer not null, prix float not null, `type` varchar(8) not null);
CREATE TABLE factures (id integer primary key autoincrement, etat integer not null, dt integer, nom varchar(50) not null, prenom varchar(50), adresse varchar(250));
CREATE TABLE lignesfactures (id integer primary key autoincrement, fid integer not null, nom varchar(250) not null, qt integer not null, prix float not null, tva float not null, `type` varchar(8) not null, remise float not null);
CREATE TABLE paiements (id integer primary key autoincrement,fid integer not null, txid varchar not null default 'internal', dt integer, montant float not null,  modepment varchar(32));
CREATE TABLE taxes (id integer primary key autoincrement,cg varchar(8) unique, libelle varchar(50) not null, tx float not null default 0, `type` varchar(8) not null);
CREATE TABLE impots(id integer primary key autoincrement,nom varchar(32) not null, dtdeb integer not null, dtfin integer not null, mtvap float not null, mtvas float not null, etat varchar(8) not null, montant float not null, annotation varchar(80));
CREATE TABLE lignesimpots(id integer primary key autoincrement, iid integer not null, libelle varchar(50) not null, `type` varchar(8) not null, montant decimal(8,2), tx decimal(5,2));
CREATE UNIQUE INDEX IF NOT EXISTS nom ON impots ( nom );
CREATE INDEX IF NOT EXISTS iid ON lignesimpots ( iid );
INSERT INTO config (vt,nom,valeur) VALUES
      (2,'nom','Exemple & co.'),
      (2,'adresse','1, Parc des menuisiers'),
      (2,'cp','12345'),
      (2,'ville','Legoland'),
      (2,'pays','France'),
      (2,'tel','0123456789'),
      (2,'email','contact@exandco.com'),
      (2,'siret','123400000000-0000-4321'),
      (2,'devis',''),
      (2,'accord','Lu et approuvé
Fait à XVille le __/__/____.'),
      (2,'comm','Commentaire devis
Multilignes'),
      (2,'modepaiement','Especes,Cheques,Paypal,C.B.');
INSERT INTO tvas (taux) VALUES (0);
INSERT INTO tvas (taux) VALUES (5.5);
INSERT INTO tvas (taux) VALUES (19.6);
INSERT INTO taxes (cg,libelle,tx,`type`) VALUES('630','Ventes de marchandises','14.3','Produit');
INSERT INTO taxes (cg,libelle,tx,`type`) VALUES('645','Presta. services commerciales ou artisanales','24.6','Service');
INSERT INTO taxes (cg,libelle,tx,`type`) VALUES('684','Autres prestation','25.1','Service');
INSERT INTO taxes (cg,libelle,tx,`type`) VALUES('574','Formation artisan','0.17','Global');
INSERT INTO taxes (cg,libelle,tx,`type`) VALUES('062','Taxe CMA vente','0.29','Produit');
INSERT INTO taxes (cg,libelle,tx,`type`) VALUES('063','Taxe CMA prestation','0.65','Service');

-- Demo --
INSERT INTO clients (nom,prenom,adresse,phone,email) VALUES ('duc','on','123, soleil
75000 Paris','0123456789', 'ducon@free.fr');
INSERT INTO produits (nom, tvaid, prix, `type`) VALUES ('Screwer',3,5.99,'Produit');
INSERT INTO produits (nom, tvaid, prix, `type`) VALUES ('Screws x 10',3,2.99,'Produit');
INSERT INTO produits (nom, tvaid, prix, `type`) VALUES ('Dépannage 1h',1,19.99,'Service');
INSERT INTO factures (etat, dt, nom , prenom, adresse) VALUES (0,1220000000,'duc','on','123, soleil
75000 Paris');
INSERT INTO lignesfactures (fid, nom, qt, prix, tva, `type`, remise) VALUES (1,'Screwer',2,5.99,19.6,'Produit',0.99);
INSERT INTO lignesfactures (fid, nom, qt, prix, tva, `type`, remise) VALUES (1,'Dépannage /h',4,24.99,0,'Service',2.99);
INSERT INTO paiements (fid,dt,montant,modepment) VALUES (1,1220100000,100,'Espece');