unit UFrameFactures;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, StdCtrls, db, ZDataset;

type

  { TFrame1 }

  TFrame1 = class(TFrame)
    Label1: TLabel;
    Label8: TLabel;
    btEditer: TButton;
    btUnEdit: TButton;
    edfChercher: TEdit;
    lbxListeFactures: TListBox;
    ZROQFactures: TZReadOnlyQuery;
    procedure btEditerClick(Sender: TObject);
    procedure btUnEditClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    FactureID: Int64;
  end; 

implementation

{$R *.lfm}

uses UCommons;
{ TFrame1 }

procedure TFrame1.btEditerClick(Sender: TObject);
begin
  FactureID := TIntObj(lbxListeFactures.Items.Objects[lbxListeFactures.ItemIndex]).Value;
  Visible := false;
end;

procedure TFrame1.btUnEditClick(Sender: TObject);
begin
//  FactureID := -1;
  Visible := false;
end;

end.

