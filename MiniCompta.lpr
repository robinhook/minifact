program MiniCompta;

{$mode objfpc}{$H+}

uses
  Classes, Forms, LCLProc, kcontrolslaz, Interfaces, UMenuPrincipal, UFactures, UClients,
  UProduits, UFrameFactures, UListeProduits, UCommons, UBilan,
  libjpfpdf, ujournal, uframepaiements,
  uImpots, uconfigure, uRegExpr, versionsupport;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmMain, FrmMain);
  Application.CreateForm(TfrmClients, frmClients);
  Application.CreateForm(TfrmProduits, frmProduits);
  Application.CreateForm(TfrmFactures, frmFactures);
  Application.CreateForm(TfrmBilan, frmBilan);
  Application.CreateForm(TfrmJournal, frmJournal);
  Application.CreateForm(TfrmConfigure, frmConfigure);
  Application.CreateForm(TfrmImpots, frmImpots);
  Application.Run;
end.
