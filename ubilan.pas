unit UBilan;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, math, dateutils, FileUtil, Forms, Controls, Graphics,
  Dialogs, StdCtrls, Menus, ZDataset, KGrids, UCommons;

type

  { TfrmBilan }

  TfrmBilan = class(TForm)
    btCalc: TButton;
    btValider: TButton;
    btEnreg: TButton;
    cbType: TComboBox;
    edDtDeb: TEdit;
    edDtFin: TEdit;
    edCG: TEdit;
    edLibelle: TEdit;
    edTaux: TEdit;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    kgtx: TKGrid;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    lbAcompte: TLabel;
    lbEncours: TLabel;
    lbAcompteService: TLabel;
    lbAcompteProduit: TLabel;
    lbFdP: TLabel;
    lbFraisDeductible: TLabel;
    lbMontant: TLabel;
    lbMontant2: TLabel;
    lbRemise: TLabel;
    lbNbFacts: TLabel;
    lbImpayee: TLabel;
    lbTva: TLabel;
    lbTvaProduit: TLabel;
    lbTvaService: TLabel;
    miEditer: TMenuItem;
    miSupprimer: TMenuItem;
    miAjouter: TMenuItem;
    pmTaxes: TPopupMenu;
    procedure btCalcClick(Sender: TObject);
    procedure btValiderClick(Sender: TObject);
    procedure btEnregClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure miAjouterClick(Sender: TObject);
    procedure miEditerClick(Sender: TObject);
    procedure miSupprimerClick(Sender: TObject);
    procedure pmTaxesPopup(Sender: TObject);
  private
    { private declarations }
    rowID: integer;
    procedure loadTaux();
  public
    { public declarations }
  end; 

var
  frmBilan: TfrmBilan;

implementation

{$R *.lfm}

{ TfrmBilan }

procedure TfrmBilan.btCalcClick(Sender: TObject);
var
  zqf, zqlf, zqp: TZReadOnlyQuery;
  sql: string;
  nbFact,i: integer;
  montant, taux: Extended;
  //Exercice chiffres globaux
    totMontant, totAcompte,
    totService, totProduit,
    totTvaService, totTvaProduit,
    totRemiseProduit, totRemiseService,
    totFImpayee,totFrais,totEnCours,totFdP,
  //Factures impots
    mtFdP,mtEnCours,mtFraisTn,txFraisTn,mtAcompte,mtMontant,mtRemiseProduit, mtRemiseService, mtProduit, mtService, mtTvaProduit, mtTvaService, ratio: Extended;
  dtdeb,dtfin: TDateTime;
  tmp:TVTRetType;
begin
  try
  if not TryStrToDateTime(edDtDeb.Text,dtdeb,BaseFact.formatters) then
    raise Exception.Create('Le champ date de début est invalide!'+#13#10+'Format accepté : dd-mm-yyyy');
  if not TryStrToDateTime(edDtFin.Text,dtfin,BaseFact.formatters) then
    raise Exception.Create('Le champ date de fin est invalide!'+#13#10+'Format accepté : dd-mm-yyyy');

  nbFact := 0;
  totMontant := 0;
  totAcompte := 0;
  totService := 0;
  totRemiseService:=0;
  totTvaProduit:=0;
  totTvaService:=0;
  totProduit := 0;
  totRemiseProduit:=0;
  totFImpayee:=0;
  totFrais:=0;
  totEnCours:=0;
  totFdP:=0;

  zqp := TZReadOnlyQuery.Create(nil);
  zqp.Connection := BaseFact.ZConn;
  zqp.SQL.Text := 'SELECT COUNT(*) FROM factures WHERE dt >= '+QuotedStr(IntToStr(DateTimeToUnix(dtdeb)))+' AND dt <= '+QuotedStr(IntToStr(DateTimeToUnix(dtfin)))+';';
  zqp.ExecSQL;
  zqp.Open;
  zqp.First;
  while not zqp.EOF do
  begin
    nbFact := zqp.Fields[0].AsInteger;
    zqp.Next;
  end;
  zqp.Close;
  zqp.Free;

  //Récup facture et paiement dans tranche date
  zqf := TZReadOnlyQuery.Create(nil);
  zqf.Connection := BaseFact.ZConn;
  sql := 'SELECT f.id, f.etat, TOTAL(p.montant) FROM factures f LEFT JOIN paiements p ON f.id=p.fid AND (p.dt >= '+QuotedStr(IntToStr(DateTimeToUnix(dtdeb)))+' AND p.dt <= '+QuotedStr(IntToStr(DateTimeToUnix(dtfin)))+') WHERE f.etat != '+IntToStr(Integer(feDevis))
        +' AND (f.dt >= '+QuotedStr(IntToStr(DateTimeToUnix(dtdeb)))+' AND f.dt <= '+QuotedStr(IntToStr(DateTimeToUnix(dtfin)))+')'
        +' OR  (p.dt >= '+QuotedStr(IntToStr(DateTimeToUnix(dtdeb)))+' AND p.dt <= '+QuotedStr(IntToStr(DateTimeToUnix(dtfin)))+')'
        +' GROUP BY f.id;';
  zqf.SQL.Text := sql;
  zqf.ExecSQL;
  zqf.Open;
  zqf.First;
  while not zqf.EOF do
  begin
    ratio := 1;
    mtProduit := 0;
    mtService := 0;
    mtTvaService := 0;
    mtTvaProduit:=0;
    mtRemiseProduit:=0;
    mtRemiseService:=0;
    mtFraisTn:=0;
    txFraisTn:=0;
    mtEnCours:=0;
    mtMontant:=0;
    mtAcompte:=0;
    mtFdP:=0;

    if zqf.Fields[2].AsCurrency > 0 then
    begin
      //Request paiements
      zqp := TZReadOnlyQuery.Create(nil);
      zqp.Connection := BaseFact.ZConn;
      zqp.SQL.Text:='SELECT fid,modepment,montant FROM paiements WHERE fid = '+zqf.Fields[0].AsString
      +' AND (dt >= '+QuotedStr(IntToStr(DateTimeToUnix(dtdeb)))+' AND dt <= '+QuotedStr(IntToStr(DateTimeToUnix(dtfin)))+');';
      zqp.ExecSQL;
      zqp.Open;
      zqp.First;
      while not zqp.EOF do
      begin
        mtFraisTn := 0;
        txFraisTn := 0;
        tmp:=BaseFact.GetDBCfgVal('vtn_'+zqp.Fields[1].AsString);
        if tmp.Value <> '' then
        begin
          if TryStrToFloat(tmp.Value,mtFraisTn,BaseFact.formatters) then
            totFrais:=totFrais+mtFraisTn;
        end;
        tmp:=BaseFact.GetDBCfgVal('txtn_'+zqp.Fields[1].AsString);
        if tmp.Value <> '' then
        begin
          if TryStrToFloat(tmp.Value,txFraisTn,BaseFact.formatters) then
          begin
            mtFraisTn := mtFraisTn + (zqp.Fields[2].AsCurrency/100*txFraisTn);
            totFrais:=totFrais+mtFraisTn;
          end;
        end;
        mtAcompte := mtAcompte + (zqp.Fields[2].AsCurrency);
        zqp.Next;
      end;
      zqp.Close;
      zqp.Free;
    end;

    //Récupération des lignes de la facture id
    zqlf := TZReadOnlyQuery.Create(nil);
    zqlf.Connection := BaseFact.ZConn;
    zqlf.SQL.Text := 'SELECT prix, tva, `type`, remise FROM lignesfactures WHERE fid = '+QuotedStr(zqf.Fields[0].AsString)+';';
    zqlf.ExecSQL;
    zqlf.Open;
    //Préparation des chiffres globaux
    zqlf.First;
    while not zqlf.EOF do
    begin
      if zqlf.Fields[2].AsString = 'Service' then
      begin
        mtService := mtService +  zqlf.Fields[0].AsFloat;

        if zqlf.Fields[1].AsFloat > 0 then
        begin
          mtTvaService := mtTvaService + ((zqlf.Fields[0].AsFloat / 100) * zqlf.Fields[1].AsFloat);
        end;
        if zqf.Fields[1].AsInteger <> Integer(feImpayee) then
          mtRemiseService := mtRemiseService + zqlf.Fields[3].AsFloat;
      end else if zqlf.Fields[2].AsString = 'Produit' then
      begin
        mtProduit := mtProduit + zqlf.Fields[0].AsFloat;

        if zqlf.Fields[1].AsFloat > 0 then
        begin
          mtTvaProduit := mtTvaProduit + ((zqlf.Fields[0].AsFloat / 100) * zqlf.Fields[1].AsFloat);
        end;
        if zqf.Fields[1].AsInteger <> Integer(feImpayee) then
          mtRemiseProduit := mtRemiseProduit + zqlf.Fields[3].AsFloat;
      end else if zqlf.Fields[2].AsString = 'FdP' then
      begin
        mtFdP := mtFdP + zqlf.Fields[0].AsFloat;
      end;
      mtMontant := mtMontant + zqlf.Fields[0].AsFloat;
      if zqf.Fields[1].AsInteger = Integer(feImpayee) then
        totFImpayee := totFImpayee + zqlf.Fields[0].AsFloat;
      zqlf.Next;
    end;

    //calcul ratio $perçu/$facturé dans le cadre de l'exercice sur la facture
    if mtAcompte = 0 then
    begin
      ratio := 0;
    end else if RoundTo(mtAcompte, -2) < RoundTo(mtService + mtProduit + mtTvaProduit + mtTvaService -(mtRemiseProduit+mtRemiseService), -2) then
    begin
      ratio := mtAcompte / (mtService + mtProduit + mtTvaProduit + mtTvaService -(mtRemiseProduit+mtRemiseService+mtFraisTn));
    end else if RoundTo(mtService + mtProduit + mtTvaProduit + mtTvaService -(mtRemiseProduit+mtRemiseService+mtFraisTn), -2) = RoundTo(mtAcompte, -2) then
    begin
      ratio := 1;
    end;

    totFdP := totFdP + (mtFdP * ratio);
    //montant facturé
    totMontant := totMontant + mtMontant;
    //acomptes perçues
    totAcompte := totAcompte + mtAcompte;
    //part services / produits
    totService := totService + (mtService * ratio);
    totProduit := totProduit + (mtProduit * ratio);
    //montant tvas
    totTvaProduit := totTvaProduit + (mtTvaProduit * ratio);
    totTvaService := totTvaService + (mtTvaService * ratio);
    //montant remises
    totRemiseService := totRemiseService + (mtRemiseService * ratio);
    totRemiseProduit := totRemiseProduit + (mtRemiseProduit * ratio);
    //calcul encours
    if zqf.Fields[1].AsInteger = Integer(feEncours) then
      mtEnCours := mtAcompte - (mtService+mtProduit);
    totEnCours := totEnCours - mtEnCours;

    zqlf.Free;
    zqf.Next;
  end;

  //Traitement des chiffres pour l'impôt
  for i := 1 to kgtx.RowCount do
  begin
    taux := 0;
    montant := 0;
    if ((TryStrToFloat(kgtx.Cells[3,i],taux)) and (taux > 0)) then
    begin
      if kgtx.Cells[5,i] = 'Service' then
      begin
        montant := ((totService - totRemiseService) / 100) * taux;
      end else if kgtx.Cells[5,i] = 'Produit' then
      begin
        montant := ((totProduit - totRemiseProduit) / 100) * taux;
      end else if kgtx.Cells[5,i] = 'Global' then
      begin
        montant := ((totService + totProduit) / 100) * taux;
      end;
      if montant > 0 then
        kgtx.Cells[4,i] := FormatFloat('0.00',montant)
      else kgtx.Cells[4,i] := 'Rien';
    end;
  end;

  lbFdP.Caption:='-'+FormatFloat('0.00',totFdP);
  lbEncours.Caption:=FormatFloat('0.00',totEncours);
  if(String(lbEncours.Caption)[1] <> '-') then lbEncours.Caption := '-'+lbEncours.Caption;
  lbFraisDeductible.Caption:='-'+FormatFloat('0.00',totFrais);
  lbNbFacts.Caption := IntToStr(nbFact);
  lbMontant.Caption := FormatFloat('0.00',totMontant);
  lbRemise.Caption := '-'+FormatFloat('0.00',totRemiseProduit + totRemiseService);
  lbImpayee.Caption := '-'+FormatFloat('0.00',totFImpayee);
  lbAcompte.Caption := FormatFloat('0.00',totAcompte);
  lbAcompteProduit.Caption := FormatFloat('0.00',totProduit - totRemiseProduit);
  lbAcompteService.Caption := FormatFloat('0.00',totService - totRemiseService);
  lbTva.Caption := FormatFloat('0.00',totTvaProduit + totTvaService);
  lbTvaProduit.Caption := FormatFloat('0.00',totTvaProduit);
  lbTvaService.Caption := FormatFloat('0.00',totTvaService);
  zqf.Free;
{*
  edTxProduit.Text := BaseFact.ConvToLocalDecSep(edTxProduit.Text);
  if (TryStrToFloat(edTxProduit.Text,txProduit) = true)  then
*}
  btEnreg.Enabled := true;
  except on e:Exception do
    ShowMessage(e.Message);
  end;
end;

procedure TfrmBilan.btValiderClick(Sender: TObject);
var zqu: TZQuery;
  query: String;
begin
  btValider.Enabled := false;

  zqu := TZQuery.Create(nil);
  zqu.Connection := BaseFact.ZConn;
  if rowID <= 0 then
    query := 'INSERT INTO taxes (cg,libelle,tx,`type`) VALUES (''%s'',''%s'',''%s'',''%s'');'
  else
    query := 'UPDATE taxes SET cg = ''%s'', libelle = ''%s'',tx = ''%s'', `type` = ''%s'' WHERE id = '+IntToStr(rowID)+';';
  zqu.SQL.Text := Format(query,[edCG.Text,edLibelle.Text,edTaux.Text,cbType.Text]);
  zqu.ExecSQL;
  zqu.Free;

  loadTaux();
end;

procedure TfrmBilan.btEnregClick(Sender: TObject);
var i, iid: integer;
  query, nom: string;
  dtdeb,dtfin: TDateTime;
  zqi: TZQuery;
  zqli: TZQuery;
begin
  nom := InputBox('Impôts','Nom du rapport','');
  if( nom <> '') then
  begin
    try
      iid := -1;

      if not TryStrToDateTime(edDtDeb.Text,dtdeb,BaseFact.formatters) then
        raise Exception.Create('Le champ date de début est invalide!'+#13#10+'Format accepté : dd-mm-yyyy');

      if not TryStrToDateTime(edDtFin.Text,dtfin,BaseFact.formatters) then
        raise Exception.Create('Le champ date de fin est invalide!'+#13#10+'Format accepté : dd-mm-yyyy');

      zqi := TZQuery.Create(nil);
      zqi.Connection := BaseFact.ZConn;

      zqli := TZQuery.Create (nil);
      zqli.Connection := BaseFact.ZConn;

      query := 'SELECT id FROM impots WHERE nom LIKE "'+nom+'" OR ( dtdeb = '+IntToStr(DateTimeToUnix(dtdeb))+' AND dtfin = '+InttoStr(DateTimeToUnix(dtfin))+') LIMIT 1;';
      zqi.SQL.Text := query;
      zqi.Open;
      zqi.First;
      if not zqi.EOF then
      begin
        iid := zqi.Fields[0].AsInteger;
        zqi.Close;

        query := 'UPDATE impots SET nom = '+QuotedStr(nom)+', dtdeb = '+IntToStr(DateTimeToUnix(dtdeb))
                 +', dtfin = '+IntToStr(DateTimeToUnix(dtfin))+', mtvap = '+QuotedStr(lbTvaProduit.Caption)
                 +', mtvas = '+QuotedStr(lbTvaService.Caption)
                 +' WHERE id = '+IntToStr(iid)+';';

      end else
      begin
        query := 'INSERT INTO impots (nom,dtdeb,dtfin,mtvap,mtvas,etat,montant,annotation) VALUES '
                +'('+QuotedStr(nom)+','+IntToStr(DateTimeToUnix(dtdeb))+','+IntToStr(DateTimeToUnix(dtfin))+','
                +QuotedStr(StringReplace(lbTvaProduit.Caption,',','.',[]))+','+QuotedStr(StringReplace(lbTvaService.Caption,',','.',[]))
                +','+QuotedStr('En cours')+',0.00,'''');';
      end;

      zqi.SQL.Text := query;
      zqi.ExecSQL;
      if zqi.RowsAffected = 0 then
        raise Exception.Create('Aucune ligne affecté!');

      if iid < 1 then
      begin
        iid := BaseFact.LastInsertID('impots');
      end else
      begin
        //Delete ligne impots
        query := 'DELETE FROM lignesimpots WHERE iid = '+IntToStr(iid)+';';
        zqli.SQL.Text := query;
        zqli.ExecSQL;
      end;

      //boucle ligne impots
      for i := 1 to kgtx.RowCount - 1 do
      begin
        query := 'INSERT INTO lignesimpots (iid,libelle,`type`,montant,tx)'
                +'VALUES('+QuotedStr(IntToStr(iid))+','
                +QuotedStr(kgtx.Cells[2,i])+','
                +QuotedStr(kgtx.Cells[5,i])+','
                +QuotedStr(StringReplace(kgtx.Cells[4,i],',','.',[]))+','
                +QuotedStr(StringReplace(kgtx.Cells[3,i],',','.',[]))+');';
        zqli.SQL.Text := query;
        zqli.ExecSQL;
      end;

      btEnreg.Enabled := false;
      except on e: Exception do
      begin
        ShowMessage(e.Message);
      end;
    end;
  end;
end;

procedure TfrmBilan.loadTaux();
var zqtx: TZReadOnlyQuery;
  r:integer;
begin
  r := 1;
  zqtx := TZReadOnlyQuery.Create(nil);
  zqtx.Connection := BaseFact.ZConn;
  zqtx.SQL.Text := 'SELECT id,cg,libelle,tx,`type` FROM taxes;';
  zqtx.ExecSQL;
  zqtx.Open;
  if zqtx.RecordCount = 0 then
    kgtx.RowCount := zqtx.RecordCount + 2
  else kgtx.RowCount := zqtx.RecordCount + 1;
  zqtx.First;
  while not zqtx.EOF do
  begin
    kgtx.Cells[0,r] := zqtx.Fields[0].AsString;
    kgtx.Cells[1,r] := zqtx.Fields[1].AsString;
    kgtx.Cells[2,r] := zqtx.Fields[2].AsString;
    kgtx.Cells[3,r] := zqtx.Fields[3].AsString;
    kgtx.Cells[4,r] := 'N/C';
    kgtx.Cells[5,r] := zqtx.Fields[4].AsString;
    inc(r);
    zqtx.Next();
  end;
  zqtx.Free;

  kgtx.Row := -1;
  edCG.Text := '';
  edTaux.Text := '';
  edLibelle.Text := '';
  cbType.ItemIndex := -1;
  btValider.Enabled := false;
end;

procedure TfrmBilan.FormCreate(Sender: TObject);
var
  cm,ca,trim:integer;
begin
  //Set fields Date
  ca := YearOf(Now());
  cm := MonthOf(Now());
  trim := ceil(cm / 3);
  {*if(trim = 0) then
  begin
    ca := ca - 1;
    trim := 3;
    cm := trim*3;
  end else*} if(trim = 1) then
  begin
    cm := 1;
  end else begin
    cm := ((trim-1) * 3)+1;
  end;
  edDtDeb.Text := '01-'+IntToStr(cm)+'-'+IntToStr(ca);
  edDtFin.Text := IntToStr(MonthDays[IsLeapYear(ca)][cm+2])+'-'+IntToStr(cm+2)+'-'+IntToStr(ca);

  //Set taxes grid
  kgtx.Cells[0,0] := 'ID';
  kgtx.Cells[1,0] := 'C.G.';
  kgtx.Cells[2,0] := 'Libellé';
  kgtx.Cells[3,0] := 'Taux';
  kgtx.Cells[4,0] := 'Montant';
  kgtx.Cells[5,0] := ''; //type

  kgtx.ColWidths[0] := 32;
  kgtx.ColWidths[1] := 38;
  kgtx.ColWidths[2] := 188;
  kgtx.ColWidths[3] := 42;
  kgtx.ColWidths[4] := 64;
  kgtx.ColWidths[5] := 0;

  loadTaux();
end;

procedure TfrmBilan.miAjouterClick(Sender: TObject);
begin
  rowID := 0;
  kgtx.RowCount := kgtx.RowCount + 1;
  kgtx.Row := kgtx.RowCount - 1;
  btValider.Enabled := true;
end;

procedure TfrmBilan.miEditerClick(Sender: TObject);
var aRow, rid: integer;
begin
  aRow := kgtx.Row;
  if not TryStrToInt(kgtx.Cells[0,aRow], rowID) then
  begin
    ShowMessage('Impossible d''éditer une ligne sans identifiant!');
    Exit;
  end;
  rowID := StrToInt(kgtx.Cells[0,aRow]);
  edCG.Text := kgtx.Cells[1,aRow];
  edLibelle.Text := kgtx.Cells[2,aRow];
  edTaux.Text := kgtx.Cells[3,aRow];
  cbType.ItemIndex := cbType.Items.IndexOf(kgtx.Cells[5,aRow]);
  btValider.Enabled := true;;
end;

procedure TfrmBilan.miSupprimerClick(Sender: TObject);
var zqd: TZQuery;
begin
  if ((rowID > 0) and (MessageDlg('Attention','Etes vous sure de vouloir supprimer cette entrée ?',mtConfirmation,[mbYes,mbNo],0) = mrYes)) then
  begin
    zqd := TZQuery.Create(nil);
    zqd.Connection := BaseFact.ZConn;
    zqd.SQL.Text := 'DELETE FROM taxes WHERE id = '+IntToStr(rowID)+';';
    zqd.ExecSQL;
    zqd.Free;
    loadTaux();
  end else if (rowID <= 0) then
    ShowMessage('Ne peut supprimer cette entrée!');
end;

procedure TfrmBilan.pmTaxesPopup(Sender: TObject);
var
  ControlCoord: TPoint;
  aCol, aRow: Integer;
  cellFnd: boolean;
begin
  ControlCoord := kgtx.ScreenToControl(pmTaxes.PopupPoint);
  cellFnd := kgtx.MouseToCell(ControlCoord.X,ControlCoord.Y,aCol,aRow);
  if cellFnd = true then
  begin
    kgtx.Row := aRow;
    rowID := StrToInt(kgtx.Cells[0,aRow]);
  end else
  begin
    kgtx.Row := -1;
    rowID := 0;
  end;
end;

end.

