unit uImpots;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, dateutils, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Grids, KGrids, KDBGrids, ZDataset, UCommons;

type

  { TfrmImpots }

  TfrmImpots = class(TForm)
    btEnreg: TButton;
    btSuppr: TButton;
    cbEtat: TComboBox;
    edAnnotation: TEdit;
    edMontant: TEdit;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    kListe: TKGrid;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    lbTvap: TLabel;
    lbTvas: TLabel;
    lbTvat: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    lbTotMontant: TLabel;
    sgImpots: TStringGrid;
    procedure btEnregClick(Sender: TObject);
    procedure btSupprClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure kListeMouseClickCell(Sender: TObject; ACol, ARow: Integer);
    procedure kListeSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
  private
    { private declarations }
    ARowListe: integer;
    zqListe:TZReadOnlyQuery;
  public
    { public declarations }
  end;

var
  frmImpots: TfrmImpots;

implementation

{$R *.lfm}

{ TfrmImpots }

procedure TfrmImpots.FormCreate(Sender: TObject);
begin
  //titre/taille colonne liste
  kListe.Cells[0,0] := 'ID';
  kListe.Cells[1,0] := 'Nom';
  kListe.Cells[2,0] := 'Date début';
  kListe.Cells[3,0] := 'Date fin';
  kListe.Cells[4,0] := 'Etat';
  kListe.Cells[5,0] := 'Montant';

  kListe.ColWidths[0] := 32;
  kListe.ColWidths[1] := 128;
  kListe.ColWidths[2] := 96;
  kListe.ColWidths[3] := 96;
  kListe.ColWidths[4] := 64;
  kListe.ColWidths[5] := 64;
  kListe.ColWidths[6] := 0;
  kListe.ColWidths[7] := 0;
  kListe.ColWidths[8] := 0;

  //Titre/taille colonnes détails
  sgImpots.Cells[0,0] := 'Libellé';
  sgImpots.Cells[1,0] := 'Type';
  sgImpots.Cells[2,0] := 'Montant';
  sgImpots.Cells[3,0] := 'Taux';

  sgImpots.ColWidths[0] := 188;
  sgImpots.ColWidths[1] := 96;
  sgImpots.ColWidths[2] := 64;
  sgImpots.ColWidths[3] := 64;

  ARowListe := 0;
end;

procedure TfrmImpots.kListeMouseClickCell(Sender: TObject; ACol, ARow: Integer);
var iid: string;
  i,idx:integer;
  totMontant: double;
  zqDetail: TZReadOnlyQuery;
begin
  if ARow = ARowListe then
    exit;

  totMontant:=0;
  iid := kListe.Cells[0,ARow];
  zqDetail := TZReadOnlyQuery.Create(nil);
  zqDetail.Connection := BaseFact.ZConn;
  zqDetail.SQL.Text := 'SELECT libelle,`type`,montant,tx FROM lignesimpots WHERE iid = '+iid+';';
  zqDetail.Open;
  zqDetail.First;
  i := 1;
  while not zqDetail.EOF do
  begin
    sgImpots.RowCount := i + 1;

    sgImpots.Cells[0,i] := zqDetail.Fields[0].AsString;
    sgImpots.Cells[1,i] := zqDetail.Fields[1].AsString;
    sgImpots.Cells[2,i] := zqDetail.Fields[2].AsString;
    sgImpots.Cells[3,i] := zqDetail.Fields[3].AsString;
    totMontant += zqDetail.Fields[2].AsFloat;

    Inc(i);
    zqDetail.Next;
  end;

  lbTvap.Caption := kListe.Cells[6,ARow];
  lbTvas.Caption := kListe.Cells[7,ARow];
  lbTvat.Caption := FloatToStr(StrToFloat(kListe.Cells[6,ARow]) + StrToFloat(kListe.Cells[7,ARow]));
  lbTotMontant.Caption := FloatToStr(totMontant);

  idx := cbEtat.Items.IndexOf(kListe.Cells[4,ARow]);
  if idx > 0 then
    cbEtat.ItemIndex := idx
    else cbEtat.ItemIndex := 0;

  edMontant.Text := kListe.Cells[5,ARow];
  edAnnotation.Text := kListe.Cells[8,ARow];

  zqDetail.Close;

  ARowListe := ARow;
end;

procedure TfrmImpots.kListeSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  if ARow <> ARowListe then
  begin
    kListeMouseClickCell(Sender, ACol, ARow);
    ARowListe := ARow;
  end;

end;

procedure TfrmImpots.FormActivate(Sender: TObject);
var i: integer;
begin
  kListe.RowCount := 2;

  zqListe := TZReadOnlyQuery.Create(nil);
  zqListe.Connection := BaseFact.ZConn;
  zqListe.SQL.Text := 'SELECT id,nom,dtdeb,dtfin,etat,montant,mtvap,mtvas,annotation FROM impots;';

  zqListe.Open;
  zqListe.First;
  i := 1;
  while not zqListe.EOF do
  begin
    kListe.RowCount := i + 1;
    //remplir grille liste
    kListe.Cells[0,i] := zqListe.Fields[0].AsString;
    kListe.Cells[1,i] := zqListe.Fields[1].AsString;

    kListe.Cells[2,i] := FormatDateTime('dd-mm-YYYY',UnixToDateTime(zqListe.Fields[2].AsLargeInt));
    kListe.Cells[3,i] := FormatDateTime('dd-mm-YYYY',UnixToDatetime(zqListe.Fields[3].AsLargeInt));

    kListe.Cells[4,i] := zqListe.Fields[4].AsString;
    kListe.Cells[5,i] := zqListe.Fields[5].AsString;
    kListe.Cells[6,i] := zqListe.Fields[6].AsString;
    kListe.Cells[7,i] := zqListe.Fields[7].AsString;
    kListe.Cells[8,i] := zqListe.Fields[8].AsString;

    Inc(i);
    zqListe.Next;
  end;
  if i > 1 then
  begin
    kListe.SelectRow(1);
    kListeMouseClickCell(Sender,0,1);
    ARowListe := 1;
  end;
  zqListe.Close;
end;

procedure TfrmImpots.btSupprClick(Sender: TObject);
var zq: TZQuery;
begin
  if MessageDlg('Supprimer','Etes-vous sûre de vouloir supprimer le rapport "'+kListe.Cells[1,ARowListe]+'"?',mtConfirmation,mbYesNo,0) = mrYes then
  begin
    zq := TZQuery.Create(nil);
    zq.Connection := BaseFact.ZConn;

    zq.SQL.Text := 'DELETE FROM lignesimpots WHERE iid = '+kListe.Cells[0,ARowListe]+';';
    zq.ExecSQL;

    zq.SQL.Text := 'DELETE FROM impots WHERE id = '+kListe.Cells[0,ARowListe]+';';
    zq.ExecSQL;

    kListe.DeleteRow(ARowListe);

    zq.Close;
  end;
  FormActivate(Sender);
end;

procedure TfrmImpots.btEnregClick(Sender: TObject);
var zq: TZQuery;
begin
  zq := TZQuery.Create(nil);
  zq.Connection := BaseFact.ZConn;

  zq.SQL.Text := 'UPDATE impots SET etat = "'+cbEtat.Items[cbEtat.ItemIndex]+'", montant = "'+BaseFact.ConvToLocalDecSep(edMontant.Text)+'" , annotation = '+QuotedStr(edAnnotation.Text)+' WHERE id = '+kListe.Cells[0,ARowListe]+';';
  zq.ExecSQL;

  zq.Close;
end;


end.

