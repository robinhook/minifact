unit UConfigure;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Grids,
  StdCtrls, ComCtrls, Menus, KGrids, UCommons, uRegExpr, Types;

type

  { TfrmConfigure }

  TfrmConfigure = class(TForm)
    btAnnuler: TButton;
    btAnnulerOther: TButton;
    btValider: TButton;
    btValiderOther: TButton;
    cbVarType: TComboBox;
    edVarName: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    mnOtherRemove: TMenuItem;
    miOtherAdd: TMenuItem;
    mmStr: TMemo;
    mmStrOther: TMemo;
    pcConfig: TPageControl;
    pmOther: TPopupMenu;
    sgConfigure: TStringGrid;
    sgOther: TStringGrid;
    tsGeneral: TTabSheet;
    tsOther: TTabSheet;
    procedure btAnnulerClick(Sender: TObject);
    procedure btAnnulerOtherClick(Sender: TObject);
    procedure btValiderClick(Sender: TObject);
    procedure btValiderOtherClick(Sender: TObject);
    procedure cbVarTypeChange(Sender: TObject);
    procedure edVarNameChange(Sender: TObject);
    procedure edVarNameExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure miOtherAddClick(Sender: TObject);
    procedure mmStrExit(Sender: TObject);
    procedure mmStrOtherExit(Sender: TObject);
    procedure mnOtherRemoveClick(Sender: TObject);
    procedure sgConfigurePrepareCanvas(sender: TObject; aCol, aRow: Integer;
      aState: TGridDrawState);
    procedure sgConfigureSelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
    procedure sgOtherPrepareCanvas(sender: TObject; aCol, aRow: Integer;
      aState: TGridDrawState);
    procedure sgOtherSelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
  private
    { private declarations }
    CurrDisplayRowIdx: integer;
    OtherCurrDisplayRowIdx: integer;
  public
    { public declarations }
  end;

var
  frmConfigure: TfrmConfigure;

implementation

{$R *.lfm}

{ TfrmConfigure }

procedure TfrmConfigure.FormCreate(Sender: TObject);
var i: integer;
begin
  cbVarType.Items.Clear;
  for i := 0 to 3 do
    cbVarType.Items.Add(BaseFact.varTypes[i]);
end;

procedure TfrmConfigure.btAnnulerClick(Sender: TObject);
begin
  Hide();
end;

procedure TfrmConfigure.btAnnulerOtherClick(Sender: TObject);
begin
  Hide();
end;

procedure TfrmConfigure.btValiderClick(Sender: TObject);
var i: integer;
  slc: TStringList;
  key,value: string;
begin
  for i := 1 to sgConfigure.RowCount - 1 do
  begin
    key := sgConfigure.Cells[0,i];
    value := sgConfigure.Cells[1,i];
    BaseFact.SetCfgVal(key,value);
  end;
  Hide();
end;

procedure TfrmConfigure.btValiderOtherClick(Sender: TObject);
var i: integer;
  slc: TStringList;
  key,value: string;
  ro:TStrRowObj;
begin
  for i := 1 to sgOther.RowCount - 1 do
  begin
    key := sgOther.Cells[0,i];
    value := sgOther.Cells[1,i];
    ro := TStrRowObj(sgOther.Objects[0,i]);
    BaseFact.SetCfgVal(key,value,ro);
  end;
end;

procedure TfrmConfigure.cbVarTypeChange(Sender: TObject);
begin
  if sgOther.Row = OtherCurrDisplayRowIdx then
  begin
{*    if sgOther.Objects[0,OtherCurrDisplayRowIdx] = nil then
    begin
      sgOther.Objects[0,OtherCurrDisplayRowIdx] := TStrRowObj.Create;
      TStrRowObj(sgOther.Objects[0,OtherCurrDisplayRowIdx]).id := 0;
    end;   *}
    TStrRowObj(sgOther.Objects[0,OtherCurrDisplayRowIdx]).vt := TVTEnum(cbVarType.ItemIndex);
  end;
end;

procedure TfrmConfigure.edVarNameChange(Sender: TObject);
begin
  if sgOther.Row = OtherCurrDisplayRowIdx then
  begin
    sgOther.Cells[0,OtherCurrDisplayRowIdx] := edVarName.Text;
{*    if sgOther.Objects[0,OtherCurrDisplayRowIdx] = nil then
    begin
      sgOther.Objects[0,OtherCurrDisplayRowIdx] := TStrRowObj.Create;
      TStrRowObj(sgOther.Objects[0,OtherCurrDisplayRowIdx]).id := 0;
    end;    *}
  end;
end;

procedure TfrmConfigure.edVarNameExit(Sender: TObject);
begin
  edVarNameChange(Sender);
end;

procedure TfrmConfigure.FormShow(Sender: TObject);
var i,i1,i2: integer;
  slc: TStringList;
  key,value: string;
begin
  i1 := 1;
  i2 := 1;
  sgConfigure.RowCount := 1;
  sgOther.RowCount := 1;
  slc := BaseFact.GetCfgValues();
  for i := 0 to slc.Count - 1 do
  begin
    slc.GetNameValue(i,key,value);
    if(slc.Objects[i]=nil) then
    begin
      sgConfigure.InsertRowWithValues(i1,[key,value]);
      Inc(i1);
    end else
    begin
      sgOther.InsertRowWithValues(i2,[key,value]);
      sgOther.Objects[0,i2] := slc.Objects[i];
      Inc(i2);
    end;
  end;
  CurrDisplayRowIdx := 1;
  sgConfigure.Row := CurrDisplayRowIdx;
  mmStr.Text := sgConfigure.Cells[1,CurrDisplayRowIdx];
  OtherCurrDisplayRowIdx := 1;
  sgOther.Row := OtherCurrDisplayRowIdx;
  cbVarType.ItemIndex := Integer(TStrRowObj(sgOther.Objects[0,OtherCurrDisplayRowIdx]).vt);
  edVarName.Text := sgOther.Cells[0,OtherCurrDisplayRowIdx];
  mmStrOther.Text := sgOther.Cells[1,OtherCurrDisplayRowIdx];
end;

procedure TfrmConfigure.miOtherAddClick(Sender: TObject);
var so: TStrRowObj;
begin
  sgOther.RowCount := sgOther.RowCount + 1;
  sgOther.Row := sgOther.RowCount - 1;
  so := TStrRowObj.Create;
  so.vt := TVTEnum(cbVarType.ItemIndex);
  so.id := 0;
  sgOther.Objects[0,sgOther.Row] := so;
end;

procedure TfrmConfigure.mmStrExit(Sender: TObject);
begin
  if sgConfigure.Row = CurrDisplayRowIdx then
    sgConfigure.Cells[1,CurrDisplayRowIdx] := mmStr.Text;
end;

procedure TfrmConfigure.mmStrOtherExit(Sender: TObject);
begin
  if sgOther.Row = OtherCurrDisplayRowIdx then
  begin
    sgOther.Cells[1,OtherCurrDisplayRowIdx] := mmStrOther.Text;
  end;
end;

procedure TfrmConfigure.mnOtherRemoveClick(Sender: TObject);
begin
  if sgOther.Row = OtherCurrDisplayRowIdx then
  begin
    BaseFact.RemCfgVal(sgOther.Cells[0,sgOther.Row],TStrRowObj(sgOther.Objects[0,sgOther.Row]));
    sgOther.DeleteRow(sgOther.Row);
  end;
end;

procedure TfrmConfigure.sgConfigurePrepareCanvas(sender: TObject; aCol,
  aRow: Integer; aState: TGridDrawState);
var ts:TTextStyle;
 re:TRegExpr;
 m:boolean;
begin
  try
    re := TRegExpr.Create;
    re.Expression := '(\r\n)+';
    re.InputString := sgConfigure.Cells[1,aRow];
    re.ModifierStr := 'gim';
    m := re.Exec;
    ts := sgConfigure.Canvas.TextStyle;
    if re.Match[0] <> '' then
    begin
      sgConfigure.RowHeights[aRow] := sgConfigure.DefaultRowHeight * (re.SubExprMatchCount+1);
      ts.SingleLine := false;
    end else
      ts.SingleLine := true;
    sgConfigure.Canvas.TextStyle := ts;

  except
    on e:Exception do
      ShowMessage(e.Message);
  end;
end;

procedure TfrmConfigure.sgConfigureSelectCell(Sender: TObject; aCol,
  aRow: Integer; var CanSelect: Boolean);
begin
  CurrDisplayRowIdx := aRow;
  mmStr.Text := sgConfigure.Cells[1,aRow];
end;

procedure TfrmConfigure.sgOtherPrepareCanvas(sender: TObject; aCol,
  aRow: Integer; aState: TGridDrawState);
var ts:TTextStyle;
 re:TRegExpr;
 m:boolean;
begin
  try
    re := TRegExpr.Create;
    re.Expression := '(\r\n)+';
    re.InputString := sgOther.Cells[1,aRow];
    re.ModifierStr := 'gim';
    m := re.Exec;
    ts := sgOther.Canvas.TextStyle;
    if re.Match[0] <> '' then
    begin
      sgOther.RowHeights[aRow] := sgOther.DefaultRowHeight * (re.SubExprMatchCount+1);
      ts.SingleLine := false;
    end else
      ts.SingleLine := true;
    sgOther.Canvas.TextStyle := ts;

  except
    on e:Exception do
      ShowMessage(e.Message);
  end;
end;

procedure TfrmConfigure.sgOtherSelectCell(Sender: TObject; aCol, aRow: Integer;
  var CanSelect: Boolean);
var idx: integer;
begin
  OtherCurrDisplayRowIdx := aRow;
  edVarName.Text := sgOther.Cells[0,aRow];
  mmStrOther.Text := sgOther.Cells[1,aRow];
  if sgOther.Objects[0,aRow] <> nil then
  begin
    idx := Integer(TStrRowObj(sgOther.Objects[0,aRow]).vt);
    if idx > -1 then
      cbVarType.ItemIndex := idx;
  end;
end;

end.

